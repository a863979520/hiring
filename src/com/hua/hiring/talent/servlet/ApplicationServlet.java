package com.hua.hiring.talent.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;

import com.hua.hiring.common.Constant;
import com.hua.hiring.common.PageModel;
import com.hua.hiring.common.util.AppUtil;
import com.hua.hiring.company.dao.PositionDao;
import com.hua.hiring.company.entity.PositionEntity;
import com.hua.hiring.talent.dao.ApplicationDao;
import com.hua.hiring.talent.dao.ResumeDao;
import com.hua.hiring.talent.entity.ApplicationEntity;
import com.hua.hiring.talent.entity.ResumeEntity;

public class ApplicationServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	private ApplicationDao applicationDao = new ApplicationDao();
	private ResumeDao resumeDao = new ResumeDao();
	private PositionDao positionDao = new PositionDao();
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String commond = request.getParameter("commond");
		if(commond != null) {
			if("delete".equals(commond)) {
				delete(request,response);
			} else if ("apply".equals(commond)) {
				apply(request,response);
			}else if ("listByTelantPage".equals(commond)) {
				listByTelantPage(request,response);
			}else if ("listByCompanyVerifyPage".equals(commond)) {
				listByCompanyVerifyPage(request,response);
			}else if ("verify".equals(commond)) {
				verify(request,response);
			}else{
				throw new IllegalStateException("请求非法");
			}
		}
	}
	/**
	 * 根据前台传来的id删除求职信息
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	private void delete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 1
		int id = Integer.parseInt(request.getParameter("id"));
		// 2
		int row = applicationDao.delete(id);
		// 3
		response.sendRedirect("TalentServlet?commond=main&row="+ row );
		
	}
	/**
	 * 根据前台传来的id投简历
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	private void apply(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 1
		int positionId = Integer.parseInt(request.getParameter("positionId"));
		int resumeId = Integer.parseInt(request.getParameter("resumeId"));
		int talentId = AppUtil.getTalentId(request, response);
		// 2
		int row  = applicationDao.apply(positionId,resumeId,talentId);
		// 3
		if(row == 1) {
			request.setAttribute("success", "操作成功~");
		}else {
			request.setAttribute("error", "操作失败~");
		}
		//
		response.sendRedirect("PositionServlet?commond=get&id=" + positionId);
		
	}
	/**
	 * 求职者根据分页来显示求职信息管理
	 */
	private void listByTelantPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int pageNo = Constant.PAGE_NO;
		//非登录状态
		String  pageNoStr = request.getParameter("pageNo");
		if(pageNoStr != null) {
			pageNo = Integer.parseInt(pageNoStr);
			// 3
		}
		//求职者简历信息
		int talentId = AppUtil.getTalentId(request, response);
		List<ResumeEntity> resumeList = resumeDao.list(talentId);
		request.setAttribute("resumeList", resumeList);
		//求职者的求职信息
		PageModel<ApplicationEntity> pageModel = applicationDao.listByTelantPage(talentId, pageNo, Constant.PAGE_SIZE);
		request.setAttribute("pageModel", pageModel);
		request.getRequestDispatcher("view/talent/main.jsp").forward(request, response);
	}
	/**
	 * 企业根据分页来显示审核求职信息
	 */
	private void listByCompanyVerifyPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//查询条件
		int companyId = AppUtil.getCompanyId(request, response);
		ApplicationEntity applicationEntity = new ApplicationEntity();
		applicationEntity.setCompany$Id(companyId);
		
		String  positionName = request.getParameter("positionName");
		if(StringUtils.isNotBlank(positionName)) {
			applicationEntity.setPosition$name(positionName);
		}
		String  statusStr = request.getParameter("status");
		if(StringUtils.isNotBlank(statusStr)) {
			int status = Integer.parseInt(statusStr);
			applicationEntity.setStatus(status);
		}
		
		int pageNo = Constant.PAGE_NO;
		//非登录状态
		String  pageNoStr = request.getParameter("pageNo");
		if(pageNoStr != null) {
			pageNo = Integer.parseInt(pageNoStr);
			// 3
		}
		List<PositionEntity> positionList = positionDao.listById(companyId);
		request.setAttribute("positionList", positionList);
		
		PageModel<ApplicationEntity> pageModel = applicationDao.listByCompanyVerifyPage(applicationEntity, pageNo, Constant.PAGE_SIZE);
		request.setAttribute("pageModel", pageModel);
		//如果是表单查询 可以把查询条件回显到前台
		request.setAttribute("applicationCondition", applicationEntity);
		
		request.getRequestDispatcher("view/company/main.jsp").forward(request, response);
	}
	/**
	 * 企业根据求职信息来审核求职状态
	 */
	private void verify(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int applicationId = Integer.parseInt(request.getParameter("applicationId"));
		int status = Integer.parseInt(request.getParameter("status"));
		int pageNo = Integer.parseInt(request.getParameter("pageNo"));
		
		applicationDao.verify(applicationId,status);
		
		response.sendRedirect("ApplicationServlet?commond=listByCompanyVerifyPage&pageNo="+ pageNo);
	}

}
