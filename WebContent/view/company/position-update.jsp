<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<%@ include file="/view/common/header-company.jspf" %>
	</head>
	<body>
		<h1 align="center">职位的修改</h1>
		<hr>
		<form action="${ctx }/PositionServlet?commond=update&id=${position.id}" method="post">
			<div class="form-group">
				<label for="name">职位名称</label> 
				<input type="text" class="form-control" id="name" name="name" value="${position.name }">
			</div>
			<div class="form-group">
				<label for="duty">职责说明</label>
				<textarea class="form-control"  rows="5" id="duty" name="duty">${position.duty }</textarea>
			</div>
			<div class="form-group">
				<label for="ability">能力要求</label>
				<textarea class="form-control" rows="5" id="ability" name="ability">${position.ability }</textarea>
			</div>
			<div class="form-group">
				<label for="salary1">薪资</label> 
				<input type="text" class="form-control" id="salary1" name="salary1" value="${position.salary1 }">
				-
				<input type="text" class="form-control" id="salary2" name="salary2" value="${position.salary2 }">
			</div>
			<button type="submit" class="btn btn-default" style="display:block;margin:0 auto">修改</button>
		</form>
		<%@ include file="/view/common/footer.jspf" %>
	</body>
</html>