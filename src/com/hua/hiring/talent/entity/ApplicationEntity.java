package com.hua.hiring.talent.entity;

import java.util.Date;

/**
 * ApplicationEntity
 *
 * @author  刘天印
 * @date  2018年11月24日下午6:55:32
 */
public class ApplicationEntity {
	private Integer id;
	private Integer positionId;
	private Integer talentId;
	private Integer resumeId;
	private Integer status;
	private Date appTime;
	private Date handleTime;
	//扩展
	private String resume$intention;
	private String position$name;
	private Integer company$Id;
	private String company$name;
	private String talent$name;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getPositionId() {
		return positionId;
	}
	public void setPositionId(Integer positionId) {
		this.positionId = positionId;
	}
	public Integer getTalentId() {
		return talentId;
	}
	public void setTalentId(Integer talentId) {
		this.talentId = talentId;
	}
	public Integer getResumeId() {
		return resumeId;
	}
	public void setResumeId(Integer resumeId) {
		this.resumeId = resumeId;
	}
	public Integer getStatus() {
		return status;
	}
	
	public String getStatusStr() {
		if(status == 1) {
			return "待审核";
		}else if(status == 2) {
			return "审核通过";
		}else if(status == 3) {
			return "已拒绝";
		}
		return "";
	}
	
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Date getAppTime() {
		return appTime;
	}
	public void setAppTime(Date appTime) {
		this.appTime = appTime;
	}
	public Date getHandleTime() {
		return handleTime;
	}
	public void setHandleTime(Date handleTime) {
		this.handleTime = handleTime;
	}
	public String getResume$intention() {
		return resume$intention;
	}
	public void setResume$intention(String resume$intention) {
		this.resume$intention = resume$intention;
	}
	public String getPosition$name() {
		return position$name;
	}
	public void setPosition$name(String position$name) {
		this.position$name = position$name;
	}
	public String getCompany$name() {
		return company$name;
	}
	public void setCompany$name(String company$name) {
		this.company$name = company$name;
	}
	public String getTalent$name() {
		return talent$name;
	}
	public void setTalent$name(String talent$name) {
		this.talent$name = talent$name;
	}
	public Integer getCompany$Id() {
		return company$Id;
	}
	public void setCompany$Id(Integer company$Id) {
		this.company$Id = company$Id;
	}
	
	
}
