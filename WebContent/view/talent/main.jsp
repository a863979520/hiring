<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<%@ include file="/view/common/header-talent.jspf" %>
	</head>
	<body>
		<h1 align="center">求职者主页</h1>
		<hr>
		<c:if test="${!empty success || !empty error}">
			<div class="alert alert-warning alert-dismissible" role="alert">
			  	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			  		<span aria-hidden="true">&times;</span>
			  	</button>
			  		<strong>${success }</strong>
			  		<strong>${error }</strong>
			</div>
		</c:if>
		<div class="bs-example" data-example-id="simple-table">
			<table class="table">
				<caption><h3 align="center">一、简历管理</h3></caption>
				<caption><h4 align="center"><a href="${ctx }/view/talent/resume-add.jsp">简历新增</a></h3></caption>
				<thead>
					<tr>
						<th>编号</th>
						<th>求职意向</th>
						<th>操作</th>
					</tr>
				</thead>
				<tbody>
					<c:if test="${empty resumeList}">
						<tr>
							<td colspan="4" align="center">还没创建简历呢~</td>
						</tr>
					</c:if>
					<c:if test="${!empty resumeList}">
						<c:forEach items="${resumeList }" var="resume" varStatus="v">
							<tr>
								<th scope="row">${v.count }</th>
								<td><a href="${ctx }/ResumeServlet?commond=preupdate&id=${resume.id}">${resume.intention }</a></td>
								<td><a href="${ctx }/ResumeServlet?commond=delete&id=${resume.id}" onclick="return confirm('是否删除？此操作不能恢复')">删除</a>
								</td>
							</tr>
						</c:forEach>
					</c:if>
				</tbody>
			</table>
	
			<table class="table">
				<caption><h3 align="center">二、求职管理</h3></caption>
				<thead>
					<tr>
						<th>编号</th>
						<th>求职意向</th>
						<th>申请职位</th>
						<th>所在公司</th>
						<th>审核时间</th>
						<th>审核状态</th>
						<th>操作</th>
					</tr>
				</thead>
				<tbody>
					<c:if test="${empty pageModel.list }">
						<tr>
							<th colspan="7" align="center">还没开始投简历呢~！</th>
						</tr>
					</c:if>
					<c:forEach items="${pageModel.list }" var="application" varStatus="v">
						<tr>
							<th scope="row">${v.count }</th>
							<td>${application.resume$intention }</td>
							<td>${application.position$name }</td>
							<td>${application.company$name }</td>
							<td><fmt:formatDate value="${application.appTime }" pattern="yyyy-MM-dd HH:mm:ss" type="date"></fmt:formatDate></td>
							<td>
								<c:if test="${application.status == 1 }">
									${application.statusStr }
								</c:if> 
								<c:if test="${application.status == 2 }">
									${application.statusStr }
									(<fmt:formatDate value="${application.handleTime }" pattern="yyyy-MM-dd HH:mm:ss" type="date"></fmt:formatDate>)
								</c:if>
								<c:if test="${application.status == 3 }">
									${application.statusStr }
									(<fmt:formatDate value="${application.handleTime }" pattern="yyyy-MM-dd HH:mm:ss" type="date"></fmt:formatDate>)
								</c:if>
							</td>
							<td><a href="${ctx }/ApplicationServlet?commond=delete&id=${application.id}" onclick="return confirm('是否撤回？此操作不能恢复')">撤回</a>
							</td>
						</tr>
					</c:forEach>
					<tr>
						<td colspan="7" align="right">
							<a href="${ctx }/ApplicationServlet?commond=listByTelantPage&pageNo=${pageModel.first}">首页</a>&nbsp;
							<a href="${ctx }/ApplicationServlet?commond=listByTelantPage&pageNo=${pageModel.pre}">上一页</a>&nbsp;
							当前第
								<select onchange="goPage(this)">
									<script type="text/javascript">
											var pageNo = '${pageModel.pageNo }';
											var totalPage = '${pageModel.totalPage }';
											for(var x = 1; x <= totalPage; x ++){
												if(pageNo == x){
													document.write('<option value="' + x + '" selected="selected">' + x +'</option>');
												}else{
													document.write('<option value="' + x + '">' + x +'</option>');
												}
											}
									</script>
								</select>页，
							共${pageModel.totalPage }页&nbsp;
							<a href="${ctx }/ApplicationServlet?commond=listByTelantPage&pageNo=${pageModel.next}">下一页</a>&nbsp;
							<a href="${ctx }/ApplicationServlet?commond=listByTelantPage&pageNo=${pageModel.last}">尾页</a>&nbsp;
							共${pageModel.allRecords }条记录
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<%@ include file="/view/common/footer.jspf" %>
		<script>
			/**
			 * 分页的点击事件
			 * @returns
			 */
			function goPage(obj) {
				location.href = '<%=request.getContextPath()%>/ApplicationServlet?commond=listByTelantPage&pageNo=' + obj.value;
			}
		</script>
	</body>
</html>