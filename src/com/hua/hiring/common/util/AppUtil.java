package com.hua.hiring.common.util;
/**
 * 当前项目的工具方法
 *
 * @author  刘天印
 * @date  2018年11月19日下午2:54:40
 */

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.hua.hiring.company.entity.CompanyEntity;
import com.hua.hiring.talent.entity.TalentEntity;

public class AppUtil {
	
	private static final Logger LOG =Logger.getLogger(AppUtil.class);
	
	public AppUtil() {
		
	}
	/**
	 * 判断用户是否登录
	 *
	 * @param: @return
	 * @return: Boolean
	 * @author: 刘天印
	 * @date: 2018年11月25日 下午4:49:08
	 */
	public static Boolean islogin(HttpServletRequest request) {
		if(request.getSession().getAttribute("talent") == null) {
			return false ;
		}else {
			return true ;
		}
	}
	/**
	 * 获取放到session里的talent对象
	 *
	 * @param: @param request
	 * @param: @return
	 * @return: TalentEntity
	 * @author: 刘天印
	 * @date: 2018年11月19日 下午3:06:13
	 */
	public static TalentEntity getTalent(HttpServletRequest request, HttpServletResponse response) {
		if(request.getSession().getAttribute("talent") == null) {
			request.setAttribute("sessionClosed", "会话已经失效或已经在其他地方登录，请重新输入");
			try {
				request.getRequestDispatcher("view/talent/talent-login.jsp").forward(request, response);
			} catch (ServletException | IOException e) {
				Util.e(LOG, e.getMessage(), e);
			}
			return null;
		}else {
			return (TalentEntity) request.getSession().getAttribute("talent");
		}
	}
	/**
	 * 获取放到session里的talent的id
	 *
	 * @param: @param request
	 * @param: @return
	 * @return: int
	 * @author: 刘天印
	 * @date: 2018年11月19日 下午3:13:36
	 */
	public static Integer getTalentId(HttpServletRequest request, HttpServletResponse response) {
		if(getTalent(request,response) == null) {
			return null;
		}else {
			return getTalent(request, response).getId();
		}
	}
	
	/**
	 * 判断企业是否登录
	 *
	 * @param: @return
	 * @return: Boolean
	 * @author: 刘天印
	 * @date: 2018年11月25日 下午4:49:08
	 */
	public static Boolean isloginc(HttpServletRequest request) {
		if(request.getSession().getAttribute("company") == null) {
			return false ;
		}else {
			return true ;
		}
	}
	/**
	 * 获取放到session里的company对象
	 *
	 * @param: @param request
	 * @param: @return
	 * @return: TalentEntity
	 * @author: 刘天印
	 * @date: 2018年11月19日 下午3:06:13
	 */
	public static CompanyEntity getCompany(HttpServletRequest request, HttpServletResponse response) {
		if(request.getSession().getAttribute("company") == null) {
			request.setAttribute("sessionClosed", "会话已经失效或已经在其他地方登录，请重新输入");
			try {
				request.getRequestDispatcher("view/company/company-login.jsp").forward(request, response);
			} catch (ServletException | IOException e) {
				Util.e(LOG, e.getMessage(), e);
			}
			return null;
		}else {
			return (CompanyEntity) request.getSession().getAttribute("company");
		}
	}
	/**
	 * 获取放到session里的company的id
	 *
	 * @param: @param request
	 * @param: @return
	 * @return: int
	 * @author: 刘天印
	 * @date: 2018年11月19日 下午3:13:36
	 */
	public static Integer getCompanyId(HttpServletRequest request, HttpServletResponse response) {
		if(getCompany(request,response) == null) {
			return null;
		}else {
			return getCompany(request, response).getId();
		}
	}
}
