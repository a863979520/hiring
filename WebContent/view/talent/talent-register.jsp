<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>求职者注册</title>
	<link rel="icon" href="<%=request.getContextPath() %>/static/common/img/favicon.ico" type="image/x-icon">
	<link rel="stylesheet" href="<%=request.getContextPath() %>/static/common/plugin/bootstrap-3.3.5/css/bootstrap.min.css">
	</head>
	<body>
		<h1 align="center">求职者注册</h1>
		<hr>
		<form action="<%=request.getContextPath() %>/TalentServlet?commond=register" method="post" onsubmit="return validate();">
			帐号：<input type="text" name="code" id="code"/><span id="codeInfo"></span><br>
			密码：<input type="text" name="password" id="password"/>
						<span id="passwordInfo"></span>
					<br>
			确认密码：<input type="text" name="password2" id="password2"/>
						<span id="password2Info"></span>
					<br>
			姓名：<input type="text" name="name" id="name"/><span id="nameInfo"></span><br>
			电话：<input type="text" name="phone" id="phone"/><span id="phoneInfo"></span><br>
			邮箱：<input type="text" name="email" id="email"/><span id="emailInfo"></span><br>
			年龄：<select name="age" id="age">
					<option value="">-请选择-</option>
					<script>
						for (var i = 10; i <= 80; i++) {
							document.write('<option value="' + i + '">' + i + '</option>');
						}
					</script>
				</select>
				<span id="ageInfo"></span><br>
			性别：<input type="radio" name="gender" value="1" checked="checked"/>男
					<input type="radio" name="gender" value="2"/>女<br>
			爱好：<input type="checkbox" name="hobby" value="看书">看书
				 <input type="checkbox" name="hobby" value="编程">编程
				 <input type="checkbox" name="hobby" value="旅游">旅游
				 <input type="checkbox" name="hobby" value="音乐">音乐
				 <span id="hobbyInfo"></span><br>
			<input type="submit" value="注册" id="submit">
		</form>
		<script src="<%=request.getContextPath() %>/static/common/plugin/jquery.min.js"></script>
		<script src="<%=request.getContextPath() %>/static/common/plugin/bootstrap-3.3.5/js/bootstrap.min.js"></script>
		<script src="<%=request.getContextPath()%>/static/talent/js/talent-register.js"></script>
		<script>
			//用jquery写
			$(function() {
			//当键盘按键被松开时触发事件:通过AJAX将input中的数据传递给后端，在后端验证是否已存在该用户名
				$("#code").keyup(
						function() {
							var code = $(this).val();
							code = $.trim(code);
							if (code != "" || code != null) {
								$.post("<%=request.getContextPath() %>/TalentServlet?commond=validateCode&code=" + code + "&p=" + new Date().getTime(),
									function(data){
										if(data == 1){
											$('#codeInfo').html("<font color='red'>该用户名已存在，请换一个再试！</font>");
											$('#submit').disabled = true;
										}else{
											$('#codeInfo').html("<font color='red'>该用户名可以使用！</font>");
											$('#submit').disabled = false;
										}
									});
							}else {
								$('#codeInfo').html("<font color='gray'>检测中...</font>");
							}
						});
			});
		</script>
		<script>
			/*用js原生写
			function validateCode() {
				var code = document.getElementById("code").value;
				if (code == '') {
					return;
				}
				
				var codeInfoDOM = document.getElementById("codeInfo");
				codeInfoDOM.innerHTML = "<font color='gray'>检测中...</font>"
					
				// 1、创建XMLHttpRequest对象（找到一个人做事）
				var xmlhttp;
				if (window.XMLHttpRequest) {
				   // code for IE7+, Firefox, Chrome, Opera, Safari
				   xmlhttp = new XMLHttpRequest();
				} else {
				   // code for IE6, IE5
				   xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				}
				
				// 2、指定XMLhttpRequest的到达地址及到达方式（告诉他做什么？去哪里做？怎么做？）
				xmlhttp.open("GET","<%=request.getContextPath() %>/TalentServlet?commond=validateCode&code=" + code + "&p=" + new Date().getTime(), true);
				
				// 3、XMLhttpRequest发送（派他出去）
				xmlhttp.send();
				
				// 4、响应结果（得到他做事的结果）
				xmlhttp.onreadystatechange = function() {
				  if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
					var submitDOM = document.getElementById('submit');
				    if (xmlhttp.responseText == 1) {
				    	codeInfoDOM.innerHTML = "<font color='red'>该用户名已存在，请换一个再试！</font>"
				    	submitDOM.disabled = true;
				    } else {
				    	codeInfoDOM.innerHTML = "<font color='green'>该用户名可以使用！</font>"
				    	submitDOM.disabled = false;
				    }
				   }
				}
			}
			*/
	</script>
	</body>
</html>