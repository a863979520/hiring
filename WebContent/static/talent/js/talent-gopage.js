/**
 * 分页的点击事件
 * @returns
 */
function goPage(obj) {
	location.href = '<%=pageContext.request.contextPath%>/ApplicationServlet?commond=listByPage&pageNo=' + obj.value;
}