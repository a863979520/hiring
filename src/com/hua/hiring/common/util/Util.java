package com.hua.hiring.common.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.log4j.Logger;

/**
 * 常用的工具类
 * @author Administrator
 *
 */
public class Util {
	private static final Logger LOG = Logger.getLogger(Util.class);

	public static final String DEFAULT_PATTERN = "yyyy-MM-dd HH:mm:ss";
	private static final String DEFAULT_PROPERTIES_NAME = "jdbc";

	private Util() {

	}

	/**
	 * Date -> String
	 * 
	 * @param date
	 * @param pattern
	 * @return
	 */
	public static String format(Date date) {
		return format(date, DEFAULT_PATTERN);
	}

	public static String format(Date date, String pattern) {
		return new SimpleDateFormat(pattern).format(date);
	}

	/**
	 * String -> Date
	 * 
	 * @param date
	 * @param pattern
	 * @return
	 * @throws ParseException
	 */
	public static Date parse(String dateStr) throws ParseException {
		return parse(dateStr, DEFAULT_PATTERN);
	}

	public static Date parse(String dateStr, String pattern)
			throws ParseException {
		return new SimpleDateFormat(pattern).parse(dateStr);
	}

	/**
	 * 读取properties文件的内容
	 * @param key
	 * @return
	 */
	public static String getDefaultBundle(String key) {
		return getBundle(DEFAULT_PROPERTIES_NAME, key);
	}

	public static String getBundle(String properitesName, String key) {
		return ResourceBundle.getBundle(properitesName).getString(key);
	}
	/**
	 * 用于对象打印，属性会在控制以多行形式显示出来
	 * 
	 * @param obj
	 */
	public static void printObject(Object obj) {
		d(LOG, ToStringBuilder.reflectionToString(obj,
				ToStringStyle.MULTI_LINE_STYLE));
	}
	/**
	 * log4j 日志输入
	 *
	 * @param: 
	 * @return: void
	 * @author: 刘天印
	 * @date: 2018年11月20日 下午6:40:55
	 */
	public static void e(Logger logger, String message,Exception e) {
		logger.error(message, e);
	}
	public static void i(Logger logger, String message) {
		if(logger.isInfoEnabled()) {
			logger.info(message);
		}
	}
	public static void d(Logger logger, String message) {
		if(logger.isDebugEnabled()) {
			logger.error(message);
		}
	}
}