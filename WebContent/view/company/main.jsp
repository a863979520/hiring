<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<%@ include file="/view/common/header-company.jspf" %>
	</head>
	<body>
		<h1 align="center">企业主页</h1>
		<hr>
		<c:if test="${!empty success || !empty error}">
			<div class="alert alert-warning alert-dismissible" role="alert">
			  	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			  		<span aria-hidden="true">&times;</span>
			  	</button>
			  		<strong>${success }</strong>
			  		<strong>${error }</strong>
			</div>
		</c:if>
		<div class="bs-example" data-example-id="simple-table">
			<table class="table">
				<caption><h3 align="center">一、职位管理</h3></caption>
				<caption>
					<h4 align="center">
						<a href="${ctx }/view/company/position-add.jsp">发布职位</a>
					</h4>
				</caption>
				<thead>
					<tr>
						<th>编号</th>
						<th>职位名称</th>
						<th>薪资</th>
						<th>发布时间</th>
						<th>操作</th>
					</tr>
				</thead>
				<tbody>
					<c:if test="${empty positionList }">
						<tr>
							<td colspan="5" align="center">贵公司还没发布职位呢~！</td>
						</tr>
					</c:if>
					<c:forEach items="${positionList }" var="position" varStatus="v">
						<tr>
							<th scope="row">${v.count }</th>
							<td><a href="${ctx }/PositionServlet?commond=preupdate&id=${position.id}">${position.name }</a></td>
							<td>${position.salary1}--${position.salary2}</td>
							<td><fmt:formatDate value="${position.releaseTime}" pattern="yyyy-MM-dd HH:mm:ss" /></td>
							<td><a href="${ctx }/PositionServlet?commond=delete&id=${position.id}" onclick="return confirm('是否删除？此操作不能恢复')">删除</a>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<table class="table">
				<caption><h3 align="center">二、审核管理</h3></caption>
				<thead>
					<tr>
						<th>编号</th>
						<th>职位名称</th>
						<th>求职者</th>
						<th>简历信息</th>
						<th>审核时间</th>
						<th>审核状态</th>
						<th>操作</th>
					</tr>
					<tr>
						<td colspan="7">
							<form id="submitForm" action="${ctx }/ApplicationServlet?commond=listByCompanyVerifyPage" method="post">
								职位名称：<input type="text" name="positionName" value="${applicationCondition.position$name }">
								审核状态：<select name="status">
										<option value="">-全部-</option>
										<option value="1" <c:if test="${applicationCondition.status == 1 }">selected=selected</c:if>>-待审核-</option>
										<option value="2" <c:if test="${applicationCondition.status == 2 }">selected=selected</c:if>>-已通过-</option>
										<option value="3" <c:if test="${applicationCondition.status == 3 }">selected=selected</c:if>>-已拒绝-</option>
									  </select>
									  
								<button>-查询-</button>
							</form>
						</td>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${pageModel.list }" var="application" varStatus="v">
						<tr>
							<th scope="row">${v.count }</th>
							<td>${application.position$name }</td>
							<td><a href="javascript:void(0)">${application.talent$name }</a></td>
							<td><a href="javascript:void(0)">${application.resume$intention }</a></td>
							<td><fmt:formatDate value="${application.appTime }" pattern="yyyy-MM-dd HH:mm:ss" type="date"></fmt:formatDate></td>
							<td>
								<c:if test="${application.status == 1 }">
									${application.statusStr }
								</c:if> 
								<c:if test="${application.status == 2 }">
									${application.statusStr }
									(<fmt:formatDate value="${application.handleTime }" pattern="yyyy-MM-dd HH:mm:ss" type="date"></fmt:formatDate>)
								</c:if>
								<c:if test="${application.status == 3 }">
									${application.statusStr }
									(<fmt:formatDate value="${application.handleTime }" pattern="yyyy-MM-dd HH:mm:ss" type="date"></fmt:formatDate>)
								</c:if>
							</td>
							<td><a href="javascript:verify(${application.id },${pageModel.pageNo })">审核</a>
							</td>
						</tr>
					</c:forEach>
					<tr>
						<td colspan="7" align="right">
							<a href="javascript:condition('${ctx }/ApplicationServlet?commond=listByCompanyVerifyPage&pageNo=${pageModel.first}')">首页</a>&nbsp;
							<a href="javascript:condition('${ctx }/ApplicationServlet?commond=listByCompanyVerifyPage&pageNo=${pageModel.pre}')">上一页</a>&nbsp;
							当前第
								<select onchange="goPage(this)">
									<script type="text/javascript">
											var pageNo = '${pageModel.pageNo }';
											var totalPage = '${pageModel.totalPage }';
											for(var x = 1; x <= totalPage; x ++){
												if(pageNo == x){
													document.write('<option value="' + x + '" selected="selected">' + x +'</option>');
												}else{
													document.write('<option value="' + x + '">' + x +'</option>');
												}
											}
									</script>
								</select>页，
							共${pageModel.totalPage }页&nbsp;
							<a href="javascript:condition('${ctx }/ApplicationServlet?commond=listByCompanyVerifyPage&pageNo=${pageModel.next}')">下一页</a>&nbsp;
							<a href="javascript:condition('${ctx }/ApplicationServlet?commond=listByCompanyVerifyPage&pageNo=${pageModel.last}')">尾页</a>&nbsp;
							共${pageModel.allRecords }条记录
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<%@ include file="/view/common/footer.jspf" %>
		<script>
			/**
			 * 分页的点击事件
			 * @returns
			 */
			function goPage(obj) {
				condition('<%=request.getContextPath()%>/ApplicationServlet?commond=listByCompanyVerifyPage&pageNo=' + obj.value);
			}
			/**
			 *审核状态的点击事件
			 */
			function verify(applicationId,pageNo) {
				
				layer.confirm('通过还是拒绝？', {
					icon: 3,
					btn: ['通过','拒绝'] //按钮
					}, function(){
						layer.msg('您选择了通过~', {icon: 1});
						location = '<%=request.getContextPath()%>/ApplicationServlet?commond=verify&status=2&applicationId=' + applicationId + '&pageNo='+ pageNo ;
					}, function(){
						layer.msg('您选择了拒绝~', {icon: 2});
						location = '<%=request.getContextPath()%>/ApplicationServlet?commond=verify&status=3&applicationId=' + applicationId + '&pageNo='+ pageNo ;
					  });
			}
			/**
			*查询事件：上一页 下一个 首页 尾页
			*/
			function condition(url) {
				var submitForm = document.getElementById('submitForm');
				submitForm.action = url;
				submitForm.submit();
			}
		</script>
		<script src="<%=request.getContextPath() %>/static/common/plugin/layer/layer.js"></script>
	</body>
</html>