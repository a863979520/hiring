package com.hua.hiring.talent.dao;

import java.util.List;

import com.hua.hiring.common.util.DBUtil;
import com.hua.hiring.talent.entity.ResumeEntity;

public class ResumeDao {
	/**
	 * 简历的新增
	 *
	 * @param: @param resumeEntity
	 * @param: @return
	 * @return: int
	 * @author: 刘天印
	 * @date: 2018年11月18日 下午3:52:33
	 * @throws RuntimeException
	 */
	public int add(ResumeEntity resumeEntity) {
		int row = DBUtil.executeUpdate("INSERT INTO resume(talent_id,intention,work_experience,project_experience) VALUE ( ?, ?, ?, ?)",resumeEntity.getTalentId(),
				resumeEntity.getIntention(),resumeEntity.getWorkExperience(),resumeEntity.getProjectExperience());
		return row ;
	}
	/**
	 * 简历的查询
	 *
	 * @param: @return
	 * @return: List<ResumeEntity>
	 * @author: 刘天印
	 * @date: 2018年11月18日 下午3:53:31
	 * @throws RuntimeException
	 */
	public List<ResumeEntity> list(int id) {
		String sql = "SELECT id, intention, work_experience, project_experience FROM resume WHERE talent_id=? ORDER BY id DESC";
		List<ResumeEntity> resumelist= DBUtil.executeQuery(ResumeEntity.class, sql, id);
		return resumelist ;
	}
	/**
	 * 简历的删除
	 *
	 * @param: @param id
	 * @param: @return
	 * @return: int
	 * @author: 刘天印
	 * @date: 2018年11月18日 下午3:53:56
	 * @throws RuntimeException
	 */
	public int delete(int id) {
		String sql = "DELETE FROM resume WHERE id=?";
		int row = DBUtil.executeUpdate(sql, id);
		return row;
	}
	/**
	 * 简历的修改（先查询到简历的详细信息方便修改）
	 *
	 * @param: @param id
	 * @param: @return
	 * @return: ResumeEntity
	 * @author: 刘天印
	 * @date: 2018年11月19日 下午5:27:57
	 */
	public ResumeEntity preUpdate(int id) {
		String sql = "SELECT id, intention, work_experience, project_experience FROM resume WHERE id=?";
		return DBUtil.getUniqueResult(ResumeEntity.class, sql, id);
	}
	/**
	 * 简历的修改（执行操作）
	 *
	 * @param: @param resumeEntity
	 * @param: @return
	 * @return: int
	 * @author: 刘天印
	 * @date: 2018年11月20日 下午3:25:08
	 */
	public int update(ResumeEntity resume) {
		String sql ="UPDATE resume SET intention=?, work_experience=?, project_experience=? WHERE id=?";
		return DBUtil.executeUpdate(sql, resume.getIntention(), resume.getWorkExperience(), resume.getProjectExperience(), resume.getId());
	}

}
