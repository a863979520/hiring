package com.hua.hiring.talent.entity;

public class ResumeEntity {
	private Integer id;
	private Integer talentId;
	private String intention;
	private String workExperience;
	private String projectExperience;

	// 
	private String talentName;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getTalentId() {
		return talentId;
	}

	public void setTalentId(Integer talentId) {
		this.talentId = talentId;
	}

	public String getIntention() {
		return intention;
	}

	public void setIntention(String intention) {
		this.intention = intention;
	}

	public String getWorkExperience() {
		return workExperience;
	}

	public void setWorkExperience(String workExperience) {
		this.workExperience = workExperience;
	}

	public String getProjectExperience() {
		return projectExperience;
	}

	public void setProjectExperience(String projectExperience) {
		this.projectExperience = projectExperience;
	}

	public String getTalentName() {
		return talentName;
	}

	public void setTalentName(String talentName) {
		this.talentName = talentName;
	}

}
