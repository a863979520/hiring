/**
 * 求职者注册表单验证
 */
function validate() {
	var flag = true;
	var code = document.getElementById('code').value;
	var password = document.getElementById('password').value;
	var password2 = document.getElementById('password2').value;
	var name = document.getElementById('name').value;
	var phone = document.getElementById('phone').value;
	var email = document.getElementById('email').value;
	var age = document.getElementById('age').value;
	var hobbies = document.getElementsByName('hobby');
	if (!/^\d{6,12}$/.test(code)) {
		document.getElementById('codeInfo').innerHTML = '帐号必须为6~12位的数字！';
		flag = false;
	} else {
		document.getElementById('codeInfo').innerHTML = '';
	}
	if (password.length < 4 || password.length > 12) {
		document.getElementById('passwordInfo').innerHTML = '密码必须为4~12！';
		flag = false;
	} else {
		document.getElementById('passwordInfo').innerHTML = '';
	}
	if (password != password2) {
		document.getElementById('password2Info').innerHTML = '两次密码输入不一致！';
		flag = false;
	} else {
		document.getElementById('password2Info').innerHTML = '';
	}
	if (name == '') {
		document.getElementById('nameInfo').innerHTML = '用户名不能为空！';
		flag = false;
	} else {
		document.getElementById('nameInfo').innerHTML = '';
	}
	if (phone == '') {
		document.getElementById('phoneInfo').innerHTML = '手机号不能为空！';
		flag = false;
	} else {
		document.getElementById('phoneInfo').innerHTML = '';
	}
	if (email == '') {
		document.getElementById('emailInfo').innerHTML = '邮箱不能为空！';
		flag = false;
	} else {
		document.getElementById('emailInfo').innerHTML = '';
	}
	if (age == '') {
		document.getElementById('ageInfo').innerHTML = '年龄不能为空！';
		flag = false;
	} else {
		document.getElementById('ageInfo').innerHTML = '';
	}
	var count = 0;
	for (var i = 0 ; i < hobbies.length; i++) {
		if (hobbies[i].checked) {
			count++;
		}
	}
	if (count < 1 || count > 3) {
		document.getElementById('hobbyInfo').innerHTML = '爱好只能有1~3个！';
		flag = false;
	} else {
		document.getElementById('hobbyInfo').innerHTML = '';
	}
	
	return flag;
}