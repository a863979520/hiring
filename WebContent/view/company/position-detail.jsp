<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" href="<%=request.getContextPath() %>/static/common/plugin/bootstrap-3.3.5/css/bootstrap.min.css">
		<title>职位的详细信息</title>
	</head>
	<body>
		<h1 align="center">职位的详细信息</h1><hr>
		<h2>职位相关信息:</h2><hr>
			职位名称 :${position.name}<br>
			职责说明 :${position.duty}<br>
			能力要求 :${position.ability}<br>
			薪       资 :${position.salary1}--${position.salary2}<br>
			发布时间 :<fmt:formatDate value="${position.releaseTime}" pattern="yyyy-MM-dd HH:mm:ss" /><br>
		<h2>公司相关信息:</h2><hr>
			企业名称 :${position.company$name}<br>
			企业邮箱 :${position.company$email}<br>
			联系电话 :${position.company$phone}<br>
			联  系  人 :${position.company$linkman}<br>
			联系地址 :${position.company$address}<br>
			人       数 :${position.company$numbers}<br>
			服务宗旨 :"${position.company$slogan}"<br>
      		<br>
      	<c:if test="${empty application }">
      		<select id="resume">
      			<option value="">-请选择简历-</option>
      			<c:forEach items="${resumeList }" var="resume">
      				<option value="${resume.id }">${resume.intention }</option>
      			</c:forEach>
      		</select>
	      	<p>
	  			<button type="button" class="btn btn-primary btn-lg" onclick="apply();">投个简历</button>
			</p>
		</c:if>
		<c:if test="${!empty application }">
			<p class="bg-success">该职位已投递，目前审核状态为：${application.statusStr }</p>
		</c:if>
		<button class="btn btn-primary btn-lg" onclick="location='<%=request.getContextPath() %>/PositionServlet?commond=list'">返回首页</button>
		<script src="<%=request.getContextPath() %>/static/common/plugin/jquery.min.js"></script>
		<script src="<%=request.getContextPath() %>/static/common/plugin/bootstrap-3.3.5/js/bootstrap.min.js"></script>
		<script type="text/javascript">
			function apply() {
				var resumeId = document.getElementById('resume').value;
				if(resumeId == ''){
					alert('请先选择简历再进行投递!');
				}else{
					location='<%=request.getContextPath() %>/ApplicationServlet?commond=apply&resumeId=' + resumeId + '&positionId=${position.id}'
				} 
			}
		</script>
	</body>
</html>