package com.hua.hiring.talent.dao;

import java.util.ArrayList;
import java.util.List;

import com.hua.hiring.common.util.DBUtil;
import com.hua.hiring.talent.entity.TalentEntity;

public class TalentDao {
	/**
	 * 求职者注册
	 * @param talentEntity
	 * @return
	 */

	public int register(TalentEntity talentEntity) {
		String sql = "INSERT INTO talent VALUES(NULL,?, ?, ?, ?, ?, ?, ?, ?)";
		List<Object> list = new ArrayList<>();
			list.add(talentEntity.getCode());
			list.add(talentEntity.getPassword());
			list.add(talentEntity.getName());
			list.add(talentEntity.getPhone());
			list.add(talentEntity.getEmail());
			list.add(talentEntity.getAge());
			list.add(talentEntity.getGender());
			list.add(talentEntity.getHobby());
		return DBUtil.executeUpdate(sql, list);
	}
	/**
	 * 求职者登录
	 * @param code
	 * @param password
	 * @return
	 */
	public TalentEntity login(String code, String password) {
		String sql = "SELECT name, id FROM talent WHERE code=? AND password=?";
		return DBUtil.getUniqueResult(TalentEntity.class, sql, code, password);
	}
	/**
	 * 求职者信息查询
	 * @return
	 */
	public TalentEntity get(int id) {
		String sql = "SELECT code, password, name, phone, email, age, gender, hobby FROM talent WHERE id =? ORDER BY id DESC";
		return DBUtil.getUniqueResult(TalentEntity.class, sql, id);
	}
	/**
	 * 验证此帐号是否已被注册
	 *
	 * @param: @param code
	 * @param: @return
	 * @return: TalentEntity
	 * @author: 刘天印
	 * @date: 2018年12月12日 下午4:52:35
	 */
	public TalentEntity validate(String code) {
		String sql ="SELECT code FROM talent WHERE code=?";
		return DBUtil.getUniqueResult(TalentEntity.class, sql, code);
	}
	/**
	 * 
	* @Description: 修改密码
	*
	* @param talentId
	* @param passw
	* @return
	* @throws：异常描述
	*
	* @author: 刘天印
	* @date: 2018年12月19日 下午8:37:09 
	*
	 */
	public int changePassword(int talentId, String passw) {
		String sql ="UPDATE company SET password=? WHERE id=?";
		return DBUtil.executeUpdate(sql, passw,talentId);
	}

}
