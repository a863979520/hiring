<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<%@ include file="/view/common/header-company.jspf" %>
	</head>
	<body>
		<h1 align="center">发布职位</h1>
		<hr>
		<form action="${ctx }/PositionServlet?commond=add" method="post">
			<div class="form-group">
				<label for="name">职位名称</label> 
				<input type="text" class="form-control" id="name" name="name" placeholder="求职意向">
			</div>
			<div class="form-group">
				<label for="duty">职责说明</label>
				<textarea class="form-control"  rows="5" id="duty" name="duty" placeholder="职责说明"></textarea>
			</div>
			<div class="form-group">
				<label for="ability">能力要求</label>
				<textarea class="form-control" rows="5" id="ability" name="ability" placeholder="能力要求"></textarea>
			</div>
			<div class="form-group">
				<label for="salary1">薪资</label> 
				<input type="text" class="form-control" id="salary1" name="salary1" placeholder="起">-
				<input type="text" class="form-control" id="salary2" name="salary2" placeholder="止">
			</div>
			<button type="submit" class="btn btn-default" style="display:block;margin:0 auto">发布</button>
		</form>
		<%@ include file="/view/common/footer.jspf" %>
	</body>
</html>