<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="icon" href="<%=request.getContextPath() %>/static/common/img/favicon.ico" type="image/x-icon">
	<link rel="stylesheet" href="<%=request.getContextPath() %>/static/common/plugin/bootstrap-3.3.5/css/bootstrap.min.css">
	<title>企业注册</title>
	</head>
	<body>
		<h1 align="center">企业注册</h1>
		<hr>
		<form class="form-horizontal" action="<%=request.getContextPath() %>/CompanyServlet?commond=register" method="post" onsubmit="return validate();">
			<div class="form-group">
			    <label for="code" class="col-sm-2 control-label">帐号</label>
			    <div class="col-sm-10">
			    	<input type="text" class="form-control" id="code" name="code" onblur="validateCode();">
			    	<span id="codeInfo"></span>
			    </div>
		  	</div>
		  	<div class="form-group">
		    	<label for="password" class="col-sm-2 control-label">密码</label>
		    	<div class="col-sm-10">
		     		<input type="password" class="form-control" id="password" name="password">
		     		<span id="passwordInfo"></span>
		    	</div>
		  	</div>
		  	<div class="form-group">
			    <label for="password2" class="col-sm-2 control-label">确认密码</label>
			    <div class="col-sm-10">
			    	<input type="password" class="form-control" id="password2" name="password2">
			    	<span id="password2Info"></span>
			    </div>
		  	</div>
		  	<div class="form-group">
			    <label for="name" class="col-sm-2 control-label">名称</label>
			    <div class="col-sm-10">
			    	<input type="text" class="form-control" id="name" name="name">
			    	<span id="nameInfo"></span>
			    </div>
		  	</div>
		  	<div class="form-group">
			    <label for="synopsis" class="col-sm-2 control-label">简介</label>
			    <div class="col-sm-10">
					<textarea class="form-control" rows="5" id="synopsis" name="synopsis"></textarea>
			    	<span id="synopsisInfo"></span>
			    </div>
		  	</div>
		  	<div class="form-group">
			    <label for="email" class="col-sm-2 control-label">邮箱</label>
			    <div class="col-sm-10">
			    	<input type="email" class="form-control" id="email" name="email">
			    	<span id="emailInfo"></span>
			    </div>
		  	</div>
		  	<div class="form-group">
			    <label for="phone" class="col-sm-2 control-label">联系电话</label>
			    <div class="col-sm-10">
			    	<input type="text" class="form-control" id="phone" name="phone">
			    	<span id="phoneInfo"></span>
			    </div>
		  	</div>
		  	<div class="form-group">
			    <label for="linkman" class="col-sm-2 control-label">联系人</label>
			    <div class="col-sm-10">
			    	<input type="text" class="form-control" id="linkman" name="linkman">
			    	<span id="linkmanInfo"></span>
			    </div>
		  	</div>
		  	<div class="form-group">
			    <label for="address" class="col-sm-2 control-label">地址</label>
			    <div class="col-sm-10">
			    	<input type="text" class="form-control" id="address" name="address">
			    	<span id="addressInfo"></span>
			    </div>
		  	</div>
		  	<div class="form-group">
			    <label for="numbers" class="col-sm-2 control-label">人数</label>
			    <div class="col-sm-10">
			    	<input type="text" class="form-control" id="numbers" name="numbers">
			    	<span id="numbersInfo"></span>
			    </div>
		  	</div>
		  	<div class="form-group">
			    <label for="slogan" class="col-sm-2 control-label">口号</label>
			    <div class="col-sm-10">
			    	<input type="text" class="form-control" id="slogan" name="slogan">
			    	<span id="sloganInfo"></span>
			    </div>
		  	</div>
		  	<div class="form-group">
		  		<div class="col-sm-offset-2 col-sm-10">
		  			<button type="submit" id="submit" class="btn btn-default" style="display:block;margin:0 auto">注册</button>
		    	</div>
			</div>
		</form>
		<script src="<%=request.getContextPath() %>/static/common/plugin/jquery.min.js"></script>
		<script src="<%=request.getContextPath() %>/static/common/plugin/bootstrap-3.3.5/js/bootstrap.min.js"></script>
		<script src="<%=request.getContextPath()%>/static/company/js/company-register.js"></script>
		<script>
			//用jquery写
			$(function() {
			//当键盘按键被松开时触发事件:通过AJAX将input中的数据传递给后端，在后端验证是否已存在该用户名
				$("#code").keyup(
						function() {
							var code = $(this).val();
							code = $.trim(code);
							if (code != "" || code != null) {
								$.post("<%=request.getContextPath() %>/TalentServlet?commond=validateCode&code=" + code + "&p=" + new Date().getTime(),
									function(data){
										if(data == 1){
											$('#codeInfo').html("<font color='red'>该用户名已存在，请换一个再试！</font>");
											$('#submit').disabled = true;
										}else{
											$('#codeInfo').html("<font color='red'>该用户名可以使用！</font>");
											$('#submit').disabled = false;
										}
									});
							}else {
								$('#codeInfo').html("<font color='gray'>检测中...</font>");
							}
						});
			});
		</script>
		<script>
			/*用js原生写的
			function validateCode() {
				var code = document.getElementById("code").value;
				if (code == '') {
					return;
				}
				
				var codeInfoDOM = document.getElementById("codeInfo");
				codeInfoDOM.innerHTML = "<font color='gray'>检测中...</font>"
					
				// 1、创建XMLHttpRequest对象（找到一个人做事）
				var xmlhttp;
				if (window.XMLHttpRequest) {
				   // code for IE7+, Firefox, Chrome, Opera, Safari
				   xmlhttp = new XMLHttpRequest();
				} else {
				   // code for IE6, IE5
				   xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				}
				
				// 2、指定XMLhttpRequest的到达地址及到达方式（告诉他做什么？去哪里做？怎么做？）
				xmlhttp.open("GET","<%=request.getContextPath() %>/CompanyServlet?commond=validateCode&code=" + code + "&p=" + new Date().getTime(), true);
				
				// 3、XMLhttpRequest发送（派他出去）
				xmlhttp.send();
				
				// 4、响应结果（得到他做事的结果）
				xmlhttp.onreadystatechange = function() {
				  if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
					var submitDOM = document.getElementById('submit');
				    if (xmlhttp.responseText == 1) {
				    	codeInfoDOM.innerHTML = "<font color='red'>该用户名已存在，请换一个再试！</font>"
				    	submitDOM.disabled = true;
				    } else {
				    	codeInfoDOM.innerHTML = "<font color='green'>该用户名可以使用！</font>"
				    	submitDOM.disabled = false;
				    }
				   }
				}
			}
			*/
	</script>
	</body>
</html>