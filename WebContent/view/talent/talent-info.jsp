<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
	<head>
	<title>求职者信息</title>
	<%@ include file="/view/common/header-talent.jspf" %>
	</head>
	<body>
		<h1 align="center">个人信息</h1>
		<hr>
			<form action="" method="post">
				帐号：<input type="text" name="code" id="code" value="${talent.code }" disabled="disabled"/><br>
				姓名：<input type="text" name="name" id="name" value="${talent.name }"/><br>
				电话：<input type="text" name="phone" id="phone" value="${talent.phone }"/><br>
				邮箱：<input type="text" name="email" id="email" value="${talent.email }"/><br>
				年龄：<select name="age" id="age">
						<option value="">-请选择-</option>
						<script>
							var age = '${talent.age}';
						</script>
						<script src="<%=request.getContextPath()%>/static/talent/js/talent-info.js"></script>
					</select><br>
				性别：<input type="radio" name="gender" value="1"/>男
					<input type="radio" name="gender" value="2"/>女<br>
				爱好：<input type="checkbox" name="hobby" value="看书">看书
				 	<input type="checkbox" name="hobby" value="编程">编程
				 	<input type="checkbox" name="hobby" value="旅游">旅游
				 	<input type="checkbox" name="hobby" value="音乐">音乐<br>
				<input type="submit" value="修改">
			</form>
		<script>
				var gender = '${talent.gender}';
				var hobby = '${talent.hobby}';
		</script>
		<%@ include file="/view/common/footer.jspf" %>
	</body>
</html>