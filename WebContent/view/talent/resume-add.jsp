<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<%@ include file="/view/common/header-talent.jspf" %>
	</head>
	<body>
		<h1 align="center">新增简历</h1>
		<hr>
		<form action="${ctx }/ResumeServlet?commond=add" method="post">
			<div class="form-group">
				<label for="intention">求职意向</label> 
				<input type="text" class="form-control" id="intention" name="intention" placeholder="求职意向">
			</div>
			<div class="form-group">
				<label for="workExperience">工作经验</label>
				<textarea class="form-control"  rows="5" id="workExperience" name="workExperience" placeholder="工作经验"></textarea>
			</div>
			<div class="form-group">
				<label for="projectExperience">项目经验</label>
				<textarea class="form-control" rows="5" id="projectExperience" name="projectExperience" placeholder="项目经验"></textarea>
			</div>
			<button type="submit" class="btn btn-default" style="display:block;margin:0 auto">新增</button>
		</form>
		<%@ include file="/view/common/footer.jspf" %>
	</body>
</html>