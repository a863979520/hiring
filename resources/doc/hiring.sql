/*
Navicat MySQL Data Transfer

Source Server         : text
Source Server Version : 50553
Source Host           : localhost:3306
Source Database       : hiring

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2018-12-19 05:29:27
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for application
-- ----------------------------
DROP TABLE IF EXISTS `application`;
CREATE TABLE `application` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `position_id` int(10) unsigned NOT NULL,
  `talent_id` int(10) unsigned NOT NULL,
  `resume_id` int(10) unsigned NOT NULL,
  `status` int(11) NOT NULL,
  `app_time` datetime NOT NULL,
  `handle_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=186 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of application
-- ----------------------------
INSERT INTO `application` VALUES ('1', '1', '1', '1', '1', '2018-12-03 15:12:47', null);
INSERT INTO `application` VALUES ('2', '2', '2', '1', '1', '2018-12-04 15:18:20', null);
INSERT INTO `application` VALUES ('3', '1', '1', '1', '1', '2018-12-03 15:12:47', null);
INSERT INTO `application` VALUES ('4', '2', '2', '1', '1', '2018-12-04 15:18:20', null);
INSERT INTO `application` VALUES ('6', '1', '1', '1', '1', '2018-12-03 15:12:47', null);
INSERT INTO `application` VALUES ('7', '2', '2', '1', '1', '2018-12-04 15:18:20', null);
INSERT INTO `application` VALUES ('8', '1', '1', '1', '1', '2018-12-03 15:12:47', null);
INSERT INTO `application` VALUES ('9', '2', '2', '1', '1', '2018-12-04 15:18:20', null);
INSERT INTO `application` VALUES ('13', '1', '1', '1', '1', '2018-12-03 15:12:47', null);
INSERT INTO `application` VALUES ('14', '2', '2', '1', '1', '2018-12-04 15:18:20', null);
INSERT INTO `application` VALUES ('15', '1', '1', '1', '1', '2018-12-03 15:12:47', null);
INSERT INTO `application` VALUES ('16', '2', '2', '1', '1', '2018-12-04 15:18:20', null);
INSERT INTO `application` VALUES ('17', '1', '1', '1', '1', '2018-12-03 15:12:47', null);
INSERT INTO `application` VALUES ('18', '2', '2', '1', '1', '2018-12-04 15:18:20', null);
INSERT INTO `application` VALUES ('19', '1', '1', '1', '1', '2018-12-03 15:12:47', null);
INSERT INTO `application` VALUES ('20', '2', '2', '1', '1', '2018-12-04 15:18:20', null);
INSERT INTO `application` VALUES ('28', '1', '1', '1', '1', '2018-12-03 15:12:47', null);
INSERT INTO `application` VALUES ('29', '2', '2', '1', '3', '2018-12-04 15:18:20', '2018-12-04 13:04:15');
INSERT INTO `application` VALUES ('30', '1', '1', '1', '1', '2018-12-03 15:12:47', null);
INSERT INTO `application` VALUES ('31', '2', '2', '1', '1', '2018-12-04 15:18:20', null);
INSERT INTO `application` VALUES ('32', '1', '1', '1', '1', '2018-12-03 15:12:47', null);
INSERT INTO `application` VALUES ('33', '2', '2', '1', '1', '2018-12-04 15:18:20', null);
INSERT INTO `application` VALUES ('34', '1', '1', '1', '1', '2018-12-03 15:12:47', null);
INSERT INTO `application` VALUES ('35', '2', '2', '1', '1', '2018-12-04 15:18:20', null);
INSERT INTO `application` VALUES ('36', '1', '1', '1', '1', '2018-12-03 15:12:47', null);
INSERT INTO `application` VALUES ('37', '2', '2', '1', '1', '2018-12-04 15:18:20', null);
INSERT INTO `application` VALUES ('38', '1', '1', '1', '1', '2018-12-03 15:12:47', null);
INSERT INTO `application` VALUES ('39', '2', '2', '1', '1', '2018-12-04 15:18:20', null);
INSERT INTO `application` VALUES ('40', '1', '1', '1', '1', '2018-12-03 15:12:47', null);
INSERT INTO `application` VALUES ('41', '2', '2', '1', '1', '2018-12-04 15:18:20', null);
INSERT INTO `application` VALUES ('42', '1', '1', '1', '1', '2018-12-03 15:12:47', null);
INSERT INTO `application` VALUES ('43', '2', '2', '1', '1', '2018-12-04 15:18:20', null);
INSERT INTO `application` VALUES ('59', '1', '1', '1', '1', '2018-12-03 15:12:47', null);
INSERT INTO `application` VALUES ('60', '2', '2', '1', '1', '2018-12-04 15:18:20', null);
INSERT INTO `application` VALUES ('61', '1', '1', '1', '1', '2018-12-03 15:12:47', null);
INSERT INTO `application` VALUES ('62', '2', '2', '1', '1', '2018-12-04 15:18:20', null);
INSERT INTO `application` VALUES ('63', '1', '1', '1', '1', '2018-12-03 15:12:47', null);
INSERT INTO `application` VALUES ('64', '2', '2', '1', '1', '2018-12-04 15:18:20', null);
INSERT INTO `application` VALUES ('65', '1', '1', '1', '1', '2018-12-03 15:12:47', null);
INSERT INTO `application` VALUES ('66', '2', '2', '1', '1', '2018-12-04 15:18:20', null);
INSERT INTO `application` VALUES ('67', '1', '1', '1', '1', '2018-12-03 15:12:47', null);
INSERT INTO `application` VALUES ('68', '2', '2', '1', '1', '2018-12-04 15:18:20', null);
INSERT INTO `application` VALUES ('69', '1', '1', '1', '1', '2018-12-03 15:12:47', null);
INSERT INTO `application` VALUES ('70', '2', '2', '1', '1', '2018-12-04 15:18:20', null);
INSERT INTO `application` VALUES ('71', '1', '1', '1', '1', '2018-12-03 15:12:47', null);
INSERT INTO `application` VALUES ('72', '2', '2', '1', '1', '2018-12-04 15:18:20', null);
INSERT INTO `application` VALUES ('73', '1', '1', '1', '1', '2018-12-03 15:12:47', null);
INSERT INTO `application` VALUES ('74', '2', '2', '1', '1', '2018-12-04 15:18:20', null);
INSERT INTO `application` VALUES ('75', '1', '1', '1', '1', '2018-12-03 15:12:47', null);
INSERT INTO `application` VALUES ('76', '2', '2', '1', '1', '2018-12-04 15:18:20', null);
INSERT INTO `application` VALUES ('77', '1', '1', '1', '1', '2018-12-03 15:12:47', null);
INSERT INTO `application` VALUES ('78', '2', '2', '1', '1', '2018-12-04 15:18:20', null);
INSERT INTO `application` VALUES ('79', '1', '1', '1', '1', '2018-12-03 15:12:47', null);
INSERT INTO `application` VALUES ('80', '2', '2', '1', '1', '2018-12-04 15:18:20', null);
INSERT INTO `application` VALUES ('81', '1', '1', '1', '1', '2018-12-03 15:12:47', null);
INSERT INTO `application` VALUES ('82', '2', '2', '1', '1', '2018-12-04 15:18:20', null);
INSERT INTO `application` VALUES ('83', '1', '1', '1', '1', '2018-12-03 15:12:47', null);
INSERT INTO `application` VALUES ('84', '2', '2', '1', '1', '2018-12-04 15:18:20', null);
INSERT INTO `application` VALUES ('85', '1', '1', '1', '1', '2018-12-03 15:12:47', null);
INSERT INTO `application` VALUES ('86', '2', '2', '1', '1', '2018-12-04 15:18:20', null);
INSERT INTO `application` VALUES ('87', '1', '1', '1', '1', '2018-12-03 15:12:47', null);
INSERT INTO `application` VALUES ('88', '2', '2', '1', '1', '2018-12-04 15:18:20', null);
INSERT INTO `application` VALUES ('89', '1', '1', '1', '1', '2018-12-03 15:12:47', null);
INSERT INTO `application` VALUES ('90', '2', '2', '1', '1', '2018-12-04 15:18:20', null);
INSERT INTO `application` VALUES ('122', '1', '1', '1', '1', '2018-12-03 15:12:47', null);
INSERT INTO `application` VALUES ('123', '2', '2', '1', '1', '2018-12-04 15:18:20', null);
INSERT INTO `application` VALUES ('124', '1', '1', '1', '1', '2018-12-03 15:12:47', null);
INSERT INTO `application` VALUES ('125', '2', '2', '1', '1', '2018-12-04 15:18:20', null);
INSERT INTO `application` VALUES ('126', '1', '1', '1', '1', '2018-12-03 15:12:47', null);
INSERT INTO `application` VALUES ('127', '2', '2', '1', '1', '2018-12-04 15:18:20', null);
INSERT INTO `application` VALUES ('128', '1', '1', '1', '1', '2018-12-03 15:12:47', null);
INSERT INTO `application` VALUES ('129', '2', '2', '1', '1', '2018-12-04 15:18:20', null);
INSERT INTO `application` VALUES ('130', '1', '1', '1', '1', '2018-12-03 15:12:47', null);
INSERT INTO `application` VALUES ('131', '2', '2', '1', '1', '2018-12-04 15:18:20', null);
INSERT INTO `application` VALUES ('132', '1', '1', '1', '1', '2018-12-03 15:12:47', null);
INSERT INTO `application` VALUES ('133', '2', '2', '1', '1', '2018-12-04 15:18:20', null);
INSERT INTO `application` VALUES ('134', '1', '1', '1', '1', '2018-12-03 15:12:47', null);
INSERT INTO `application` VALUES ('135', '2', '2', '1', '1', '2018-12-04 15:18:20', null);
INSERT INTO `application` VALUES ('136', '1', '1', '1', '1', '2018-12-03 15:12:47', null);
INSERT INTO `application` VALUES ('137', '2', '2', '1', '1', '2018-12-04 15:18:20', null);
INSERT INTO `application` VALUES ('138', '1', '1', '1', '1', '2018-12-03 15:12:47', null);
INSERT INTO `application` VALUES ('139', '2', '2', '1', '1', '2018-12-04 15:18:20', null);
INSERT INTO `application` VALUES ('140', '1', '1', '1', '1', '2018-12-03 15:12:47', null);
INSERT INTO `application` VALUES ('141', '2', '2', '1', '1', '2018-12-04 15:18:20', null);
INSERT INTO `application` VALUES ('142', '1', '1', '1', '1', '2018-12-03 15:12:47', null);
INSERT INTO `application` VALUES ('143', '2', '2', '1', '1', '2018-12-04 15:18:20', null);
INSERT INTO `application` VALUES ('144', '1', '1', '1', '1', '2018-12-03 15:12:47', null);
INSERT INTO `application` VALUES ('145', '2', '2', '1', '1', '2018-12-04 15:18:20', null);
INSERT INTO `application` VALUES ('146', '1', '1', '1', '1', '2018-12-03 15:12:47', null);
INSERT INTO `application` VALUES ('147', '2', '2', '1', '1', '2018-12-04 15:18:20', null);
INSERT INTO `application` VALUES ('148', '1', '1', '1', '1', '2018-12-03 15:12:47', null);
INSERT INTO `application` VALUES ('149', '2', '2', '1', '1', '2018-12-04 15:18:20', null);
INSERT INTO `application` VALUES ('150', '1', '1', '1', '1', '2018-12-03 15:12:47', null);
INSERT INTO `application` VALUES ('151', '2', '2', '1', '1', '2018-12-04 15:18:20', null);
INSERT INTO `application` VALUES ('152', '1', '1', '1', '1', '2018-12-03 15:12:47', null);
INSERT INTO `application` VALUES ('153', '2', '2', '1', '1', '2018-12-04 15:18:20', null);
INSERT INTO `application` VALUES ('154', '1', '1', '1', '1', '2018-12-03 15:12:47', null);
INSERT INTO `application` VALUES ('155', '2', '2', '1', '1', '2018-12-04 15:18:20', null);
INSERT INTO `application` VALUES ('156', '1', '1', '1', '1', '2018-12-03 15:12:47', null);
INSERT INTO `application` VALUES ('157', '2', '2', '1', '1', '2018-12-04 15:18:20', null);
INSERT INTO `application` VALUES ('158', '1', '1', '1', '1', '2018-12-03 15:12:47', null);
INSERT INTO `application` VALUES ('159', '2', '2', '1', '1', '2018-12-04 15:18:20', null);
INSERT INTO `application` VALUES ('160', '1', '1', '1', '1', '2018-12-03 15:12:47', null);
INSERT INTO `application` VALUES ('161', '2', '2', '1', '1', '2018-12-04 15:18:20', null);
INSERT INTO `application` VALUES ('162', '1', '1', '1', '1', '2018-12-03 15:12:47', null);
INSERT INTO `application` VALUES ('163', '2', '2', '1', '1', '2018-12-04 15:18:20', null);
INSERT INTO `application` VALUES ('164', '1', '1', '1', '1', '2018-12-03 15:12:47', null);
INSERT INTO `application` VALUES ('165', '2', '2', '1', '1', '2018-12-04 15:18:20', null);
INSERT INTO `application` VALUES ('166', '1', '1', '1', '1', '2018-12-03 15:12:47', null);
INSERT INTO `application` VALUES ('167', '2', '2', '1', '1', '2018-12-04 15:18:20', null);
INSERT INTO `application` VALUES ('168', '1', '1', '1', '1', '2018-12-03 15:12:47', null);
INSERT INTO `application` VALUES ('169', '2', '2', '1', '2', '2018-12-04 15:18:20', '2018-12-03 18:15:21');
INSERT INTO `application` VALUES ('170', '1', '1', '1', '1', '2018-12-03 15:12:47', null);
INSERT INTO `application` VALUES ('171', '2', '2', '1', '1', '2018-12-04 15:18:20', null);
INSERT INTO `application` VALUES ('172', '1', '1', '1', '1', '2018-12-03 15:12:47', null);
INSERT INTO `application` VALUES ('173', '2', '2', '1', '1', '2018-12-04 15:18:20', null);
INSERT INTO `application` VALUES ('174', '1', '1', '1', '1', '2018-12-03 15:12:47', null);
INSERT INTO `application` VALUES ('175', '2', '2', '1', '3', '2018-12-04 15:18:20', '2018-12-04 13:03:13');
INSERT INTO `application` VALUES ('176', '1', '1', '1', '3', '2018-12-03 15:12:47', '2018-12-03 18:15:10');
INSERT INTO `application` VALUES ('177', '2', '2', '1', '1', '2018-12-04 15:18:20', null);
INSERT INTO `application` VALUES ('178', '1', '1', '1', '1', '2018-12-03 15:12:47', null);
INSERT INTO `application` VALUES ('179', '2', '2', '1', '1', '2018-12-04 15:18:20', null);
INSERT INTO `application` VALUES ('180', '1', '1', '1', '1', '2018-12-03 15:12:47', null);
INSERT INTO `application` VALUES ('181', '2', '2', '1', '1', '2018-12-04 15:18:20', null);
INSERT INTO `application` VALUES ('182', '1', '1', '1', '2', '2018-12-03 15:12:47', '2018-12-04 13:03:07');
INSERT INTO `application` VALUES ('183', '2', '2', '1', '1', '2018-12-04 15:18:20', null);
INSERT INTO `application` VALUES ('184', '1', '1', '1', '1', '2018-12-03 15:12:47', null);
INSERT INTO `application` VALUES ('185', '2', '2', '1', '3', '2018-12-04 15:18:20', '2018-12-04 12:49:26');

-- ----------------------------
-- Table structure for company
-- ----------------------------
DROP TABLE IF EXISTS `company`;
CREATE TABLE `company` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(50) NOT NULL,
  `synopsis` text,
  `email` varchar(50) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `linkman` varchar(20) NOT NULL,
  `address` varchar(100) NOT NULL,
  `numbers` int(11) NOT NULL,
  `slogan` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of company
-- ----------------------------
INSERT INTO `company` VALUES ('1', '123456', '2093BEFCABA2C5AA0EAA078C005B42A3', '重庆上善良云图信息技术有限公司', '0000', '13389667698@qq.com', '759694429@163.com', '张韬', '重庆沙坪坝西永微电园', '350', '资源共享，成就价值！');

-- ----------------------------
-- Table structure for position
-- ----------------------------
DROP TABLE IF EXISTS `position`;
CREATE TABLE `position` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(10) unsigned NOT NULL,
  `name` varchar(50) NOT NULL,
  `duty` varchar(1000) NOT NULL,
  `ability` varchar(1000) NOT NULL,
  `salary1` int(11) NOT NULL,
  `salary2` int(11) NOT NULL,
  `release_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of position
-- ----------------------------
INSERT INTO `position` VALUES ('1', '1', '培训讲师', '主要负责Java和Android方向的教学', '精通Java，精通Android，精通前端', '7000', '15000', '2018-12-03 15:12:47');
INSERT INTO `position` VALUES ('2', '1', 'php', '000', '000', '100', '10000', '2018-12-03 15:19:16');

-- ----------------------------
-- Table structure for resume
-- ----------------------------
DROP TABLE IF EXISTS `resume`;
CREATE TABLE `resume` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `talent_id` int(10) unsigned NOT NULL,
  `intention` varchar(200) NOT NULL,
  `work_experience` text NOT NULL,
  `project_experience` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of resume
-- ----------------------------
INSERT INTO `resume` VALUES ('1', '1', '培训讲师', '曾任xxx公司总裁、经理、开发者、保洁人员', '让xxx公司成功上市！');
INSERT INTO `resume` VALUES ('2', '2', 'java', '000', '000');
INSERT INTO `resume` VALUES ('3', '1', 'php', '11', '111');
INSERT INTO `resume` VALUES ('4', '1', 'JAVA开发老司机', '2', '2');

-- ----------------------------
-- Table structure for talent
-- ----------------------------
DROP TABLE IF EXISTS `talent`;
CREATE TABLE `talent` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(45) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `age` int(11) NOT NULL,
  `gender` int(11) NOT NULL,
  `hobby` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of talent
-- ----------------------------
INSERT INTO `talent` VALUES ('1', '111111', '9878F729570C17C12C7D1812F7C7D4CB', '谢帅', '13389667698', '759694429@qq.com', '23', '1', 'Java|PHP');
INSERT INTO `talent` VALUES ('2', '222222', '22DFB716057217C42424C3F6671589A4', '李二', '15826166655', '8636666654@qq.com', '20', '1', 'php');
