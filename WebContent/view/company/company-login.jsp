<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="icon" href="<%=request.getContextPath() %>/static/common/img/favicon.ico" type="image/x-icon">
		<link rel="stylesheet" href="<%=request.getContextPath() %>/static/common/plugin/bootstrap-3.3.5/css/bootstrap.min.css">
		<title>企业登录</title>
	</head>
	<body>
		<h1 align="center">企业登录</h1> 
			<c:if test="${!empty errorInfo || !empty sessionClosed}">
				<div class="alert alert-danger alert-dismissible fade in" role="alert">
			      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
			      	<span aria-hidden="true">×</span>
			      </button>
			      <strong>${errorInfo }${sessionClosed }</strong>
			    </div>
	    	</c:if>
			<form action="<%=request.getContextPath() %>/CompanyServlet?commond=login" method="post">
				<div class="form-group">
		    		<label for="code">帐号：</label>
		    		<input type="text" class="form-control" id="code" name="code" value="123456" autocomplete="off" placeholder="帐号...">
		  		</div>
		  		<div class="form-group">
		    		<label for="password">密码：</label>
		    		<input type="password" class="form-control" id="password" name="password" value="123456" autocomplete="off" placeholder="密码...">
		  		</div>
		  		<input type="text" name="verifyCode" id="verifyCode" width="100px" height="100px" autocomplete="off">
                    <img src="<%=request.getContextPath() %>/Kaptcha.jpg" onclick="changeVerifyCode()" id="yzmImg" style="cursor: pointer;" title="看不清，换一种">
		  		<div class="checkbox">
		    	<label>
		      		<input type="checkbox"> 记住密码
		   	 	</label>
		  		</div>
		  			<button type="submit" class="btn btn-default">Login</button>
		  			<input type="button" class="btn btn-default" value="注册" onclick='window.open("view/company/company-register.jsp")'>
		  			<input type="button" class="btn btn-default" value="回到首页" onclick="window.open('/hiring/index.jsp')">
			</form>
		<script src="<%=request.getContextPath() %>/static/common/plugin/jquery.min.js"></script>
		<script src="<%=request.getContextPath() %>/static/common/plugin/bootstrap-3.3.5/js/bootstrap.min.js"></script>
		<script>
    		//点击切换验证码
        	function changeVerifyCode(){
            	$("#yzmImg").attr("src","<%=request.getContextPath() %>/Kaptcha.jpg?"+Math.floor(Math.random()*100));
       		}
		</script>
	</body>
</html>