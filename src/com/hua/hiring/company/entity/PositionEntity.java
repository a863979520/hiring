package com.hua.hiring.company.entity;

import java.util.Date;


public class PositionEntity {
	
	private Integer id;
	private Integer companyId;
	private String name;
	private String duty;
	private String ability;
	private Integer salary1;
	private Integer salary2;
	private Date releaseTime;
	
	/**
	 * 扩展属性
	 */
	private String company$name;
	private String company$email;
	private String company$phone;
	private String company$linkman;
	private String company$address;
	private String company$numbers;
	private String company$slogan;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDuty() {
		return duty;
	}
	public void setDuty(String duty) {
		this.duty = duty;
	}
	public String getAbility() {
		return ability;
	}
	public void setAbility(String ability) {
		this.ability = ability;
	}
	public Integer getSalary1() {
		return salary1;
	}
	public void setSalary1(Integer salary1) {
		this.salary1 = salary1;
	}
	public Integer getSalary2() {
		return salary2;
	}
	public void setSalary2(Integer salary2) {
		this.salary2 = salary2;
	}
	public Date getReleaseTime() {
		return releaseTime;
	}
	public void setReleaseTime(Date releaseTime) {
		this.releaseTime = releaseTime;
	}
	public String getCompany$name() {
		return company$name;
	}
	public void setCompany$name(String company$name) {
		this.company$name = company$name;
	}
	public String getCompany$address() {
		return company$address;
	}
	public void setCompany$address(String company$address) {
		this.company$address = company$address;
	}
	public String getCompany$slogan() {
		return company$slogan;
	}
	public void setCompany$slogan(String company$slogan) {
		this.company$slogan = company$slogan;
	}
	public String getCompany$email() {
		return company$email;
	}
	public void setCompany$email(String company$email) {
		this.company$email = company$email;
	}
	public String getCompany$phone() {
		return company$phone;
	}
	public void setCompany$phone(String company$phone) {
		this.company$phone = company$phone;
	}
	public String getCompany$linkman() {
		return company$linkman;
	}
	public void setCompany$linkman(String company$linkman) {
		this.company$linkman = company$linkman;
	}
	public String getCompany$numbers() {
		return company$numbers;
	}
	public void setCompany$numbers(String company$numbers) {
		this.company$numbers = company$numbers;
	}
	
}
