package com.hua.hiring.company.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hua.hiring.common.Constant;
import com.hua.hiring.common.util.AESUtil;
import com.hua.hiring.common.util.AppUtil;
import com.hua.hiring.company.dao.CompanyDao;
import com.hua.hiring.company.entity.CompanyEntity;

public class CompanyServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	private CompanyDao companyDao = new CompanyDao();
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String commond = request.getParameter("commond");
		if(commond != null) {
			if("login".equals(commond)){
				login(request, response);
			}else if("logout".equals(commond)){
				logout(request, response);
			}else if("main".equals(commond)){
				main(request, response);
			}else if("register".equals(commond)){
				register(request, response);
			}else if("validateCode".equals(commond)){
				validateCode(request, response);
			}else if("detail".equals(commond)){
				detail(request, response);
			}else if("update".equals(commond)) {
				update(request,response);
			}else if("changePassword".equals(commond)) {
				changePassword(request,response);
			}else{
				throw new IllegalStateException("请求非法");
			}
		}
	}
	/**
	 * 企业登录
	 */
	private void login(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
			//1
			String code = request.getParameter("code");
			String passwordStr = request.getParameter("password");
			String verifyCode = request .getParameter("verifyCode");
			//获取kaptcha生成存放在session中的验证码
	        String kaptchaValue = (String) request.getSession().getAttribute(com.google.code.kaptcha.Constants.KAPTCHA_SESSION_KEY);
			if(verifyCode == null || verifyCode == "") {
				request.setAttribute("errorInfo", "请输入验证码");
				request.getRequestDispatcher("view/company/company-login.jsp").forward(request, response);
			}
			if(!verifyCode.equals(kaptchaValue)) {
				request.setAttribute("errorInfo", "验证码错误，请重新输入验证码");
				request.getRequestDispatcher("view/company/company-login.jsp").forward(request, response);
			}
			//1.3得到相对应的加密密码 
			byte[] passwo = (AESUtil.encrypt(passwordStr,Constant.AES_KEY));
			String password = AESUtil.parseByte2HexStr(passwo);
			//2
			CompanyEntity company = companyDao.login(code,password);
			//3
			if(company == null) {
				request.setAttribute("code", code);
				request.setAttribute("errorInfo", "用户名或密码错误");
				request.getRequestDispatcher("view/company/company-login.jsp").forward(request, response);
			}else {
				request.getSession().setAttribute("company", company);
				//另一个session会话 用于单态登录
				request.getSession().setAttribute("companyCode", code);
				response.sendRedirect("CompanyServlet?commond=main");
				}
	}
	/**
	 * 企业注销
	 */
	private void logout(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getSession().invalidate();
		request.getRequestDispatcher("view/company/company-login.jsp").forward(request, response);
	}
	/**
	 * 企业主页
	 */
	private void main(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String rowStr = request.getParameter("row");
			//登录时为空
			if (rowStr != null) {
				int row = Integer.parseInt(rowStr);
				if(row == 1){
					request.setAttribute("success", "操作成功~");
				} else {
					request.setAttribute("error", "操作失败~");
				}
			}
		request.getRequestDispatcher("ApplicationServlet?commond=listByCompanyVerifyPage").forward(request, response);
	}
	/**
	 * 企业注册
	 *
	 * @param: @param request
	 * @param: @param response
	 * @param: @throws ServletException
	 * @param: @throws IOException
	 * @return: void
	 * @author: 刘天印
	 * @date: 2018年11月30日 下午7:10:47
	 */
	private void register(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//1
			String code = request.getParameter("code");
			String password = request.getParameter("password");
			String name = request.getParameter("name");
			String synopsis = request.getParameter("synopsis");
			String phone = request.getParameter("phone");
			String email = request.getParameter("email");
			String linkman = request.getParameter("linkman");
			String address = request.getParameter("address");
			Integer numbers = Integer.parseInt(request.getParameter("numbers"));
			String slogan = request.getParameter("slogan");
		//2
			CompanyEntity companyEntity = new CompanyEntity();
			companyEntity.setCode(code);
			//加密
			byte[] passwo = AESUtil.encrypt(password,Constant.AES_KEY);
			String pass = AESUtil.parseByte2HexStr(passwo);
			companyEntity.setPassword(pass);
			
			companyEntity.setName(name);
			companyEntity.setSynopsis(synopsis);
			companyEntity.setPhone(phone);
			companyEntity.setEmail(email);
			companyEntity.setLinkman(linkman);
			companyEntity.setAddress(address);
			companyEntity.setNumbers(numbers);
			companyEntity.setSlogan(slogan);
			companyDao.regeister(companyEntity);
		
		//3
		request.getRequestDispatcher("view/company/company-login.jsp").forward(request, response);
	}
	/**
	 * 运用AJAX查询了此帐号是否已别占用
	 */
	private void validateCode(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//1
		String code = request.getParameter("code");
		//2
		CompanyEntity company = companyDao.validateCode(code);
		//3 IO流
		PrintWriter printWriter = response.getWriter();
		try {
			if(company == null) {
				printWriter.print(0);
			}else {
				printWriter.print(1);
			}
		} finally {
			printWriter.close();
		}
}
	/**
	 * 公司的信息（为修改做铺垫）
	 *
	 * @param: @param request
	 * @param: @param response
	 * @param: @throws ServletException
	 * @param: @throws IOException
	 * @return: void
	 * @author: 刘天印
	 * @date: 2018年12月2日 下午3:44:46
	 */
	private void detail(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int companyId = Integer.parseInt(request.getParameter("id"));
		CompanyEntity company = companyDao.detail(companyId);
		//解密
		byte[] pass = AESUtil.parseHexStr2Byte(company.getPassword());
		byte[] password = AESUtil.decrypt(pass, Constant.AES_KEY);
		company.setPassword(new String(password));
		
		request.setAttribute("company", company);
		request.getRequestDispatcher("view/company/company-info.jsp").forward(request, response);
	}
	/**
	 * 公司信息的修改
	 *
	 * @param: @param request
	 * @param: @param response
	 * @param: @throws ServletException
	 * @param: @throws IOException
	 * @return: void
	 * @author: 刘天印
	 * @date: 2018年12月2日 下午3:58:05
	 */
	private void update(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 1
		int id = AppUtil.getCompanyId(request, response);
		String code = request.getParameter("code");
		String name = request.getParameter("name");
		String synopsis = request.getParameter("synopsis");
		String email = request.getParameter("email");
		String phone = request.getParameter("phone");
		String linkman = request.getParameter("linkman");
		String address = request.getParameter("address");
		int numbers = Integer.parseInt(request.getParameter("numbers"));
		String slogan = request.getParameter("slogan");
		//
		CompanyEntity companyEntity = new CompanyEntity();
		companyEntity.setId(id);
		companyEntity.setCode(code);
		companyEntity.setName(name);
		companyEntity.setSynopsis(synopsis);
		companyEntity.setEmail(email);
		companyEntity.setPhone(phone);
		companyEntity.setLinkman(linkman);
		companyEntity.setAddress(address);
		companyEntity.setNumbers(numbers);
		companyEntity.setSlogan(slogan);
		int row = companyDao.update(companyEntity);
		// 3
		response.sendRedirect("CompanyServlet?commond=main&row="+ row );
	}
	/**
	 * 
	* @Description: 修改密码
	*
	* @param request
	* @param response
	* @throws ServletException
	* @throws IOException
	* @throws：异常描述
	*
	* @author: 刘天印
	* @date: 2018年12月19日 下午4:48:45 
	*
	 */
	private void changePassword(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int companyId = Integer.parseInt(request.getParameter("id"));
		String passwordStr = request.getParameter("pass"); 
		//加密
		byte[] passwo = AESUtil.encrypt(passwordStr,Constant.AES_KEY);
		String passw = AESUtil.parseByte2HexStr(passwo);
		
		int row = companyDao.changePassword(companyId,passw);
		
		PrintWriter printWriter = response.getWriter();
		try {
			if (row == 1) {
					printWriter.print(1);
				} else {
					printWriter.print(0);
				}
		} finally {
			printWriter.close();
		}
		
	}
}
