package com.hua.hiring.talent.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hua.hiring.common.util.AppUtil;
import com.hua.hiring.talent.dao.ResumeDao;
import com.hua.hiring.talent.entity.ResumeEntity;

public class ResumeServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	private ResumeDao resumeDao = new ResumeDao();
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String commond = request.getParameter("commond");
		if(commond != null) {
			if("add".equals(commond)){
				add(request, response);
			} else if("delete".equals(commond)) {
				delete(request,response);
			} else if("preupdate".equals(commond)) {
				preupdate(request,response);
			} else if("update".equals(commond)) {
				update(request,response);
			} else{
				throw new IllegalStateException("请求非法");
			}
		}
	}
	/**
	 * 简历的新增
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	private void add(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int talentId = AppUtil.getTalentId(request, response);
		String intention = request.getParameter("intention");
		String workExperience = request.getParameter("workExperience");
		String projectExperience = request.getParameter("projectExperience");
		//
		ResumeEntity resumeEntity = new ResumeEntity();
		resumeEntity.setTalentId(talentId);
		resumeEntity.setIntention(intention);
		resumeEntity.setWorkExperience(workExperience);
		resumeEntity.setProjectExperience(projectExperience);
		int row = resumeDao.add(resumeEntity);
		// 3
		response.sendRedirect("TalentServlet?commond=main&row="+ row );
	}
	/**
	 * 简历的删除
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	private void delete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 1
		int id = Integer.parseInt(request.getParameter("id"));
		// 2
		int row = resumeDao.delete(id);
		// 3
		response.sendRedirect("TalentServlet?commond=main&row="+ row );
	}
	/**
	 * 简历的修改（先查询到简历的详细信息再修改）
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	private void preupdate(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 1
		int id = Integer.parseInt(request.getParameter("id"));
		// 2
		ResumeEntity resume = resumeDao.preUpdate(id);
		// 3
		if(resume == null) {
			request.setAttribute("error", "该简历不存在或用户操作失误~");
			response.sendRedirect("TalentServlet?commond=main");
		}else {
			request.setAttribute("resume", resume);
			request.getRequestDispatcher("view/talent/resume-update.jsp").forward(request, response);
		}
		
	}
	/**
	 * 简历的修改（执行操作）
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	private void update(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 1
		int id = Integer.parseInt(request.getParameter("id"));
		String intention = request.getParameter("intention");
		String workExperience = request.getParameter("workExperience");
		String projectExperience = request.getParameter("projectExperience");

		//
		ResumeEntity resumeEntity = new ResumeEntity();
		resumeEntity.setId(id);
		resumeEntity.setIntention(intention);
		resumeEntity.setWorkExperience(workExperience);
		resumeEntity.setProjectExperience(projectExperience);
		int row = resumeDao.update(resumeEntity);
		// 3
		response.sendRedirect("TalentServlet?commond=main&row="+ row );
	}

}
