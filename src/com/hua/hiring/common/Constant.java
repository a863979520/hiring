package com.hua.hiring.common;

import com.hua.hiring.common.util.Util;
/**
 * 系统常量
 *
 * @author  刘天印
 * @date  2018年11月28日下午2:50:51
 */
public class Constant {

	/**
	 * 数据库相关的常量
	 */
	public static final String DRIVER = Util.getDefaultBundle("driver");
	public static final String URL = Util.getDefaultBundle("url");
	public static final String USER = Util.getDefaultBundle("user");
	public static final String PASSWORD = Util.getDefaultBundle("password");
	/**
	 * 分页的相关常量
	 */
	public static final Integer PAGE_SIZE = 10;
	public static final Integer PAGE_NO = 1;
	/**
	 * AES的密钥（用于加密与解密）
	 */
	public static final String AES_KEY = "ASd154484564";
}