<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>简历新增成功</title>
</head>
<body>
	<table border="1" width="100%">
		<tr>
			<th>求职者</th>
			<th>求职意向</th>
			<th>工作经验</th>
			<th>项目经验</th>
		</tr>
		<c:forEach items="${resuemlist}" var="resuemlist">
			<tr>
				<td>${resuemlist.talentName}</td>
				<td>${resuemlist.intention}</td>
				<td>${resuemlist.workExperience}</td>
				<td>${resuemlist.projectExperience}</td>
			</tr>
		</c:forEach>
	</table>
</body>
</html>