package com.hua.hiring.common;
/**
 * 分页的实体
 *
 * @author  刘天印
 * @date  2018年11月28日下午3:35:08
 */

import java.util.List;

public class PageModel<T> {
	
	//每次查询到的数据存放
	private List<T> list;
	//第几页
	private Integer pageNo;
	//每页显示多少条
	private Integer pageSize;
	//共多少条记录 在sql语句里写 select count(*) from 表
	private Integer allRecords;
	
	
	public PageModel() {
		super();
	}
	public PageModel(List<T> list, Integer pageNo, Integer pageSize, Integer allRecords) {
		super();
		this.list = list;
		this.pageNo = pageNo;
		this.pageSize = pageSize;
		this.allRecords = allRecords;
	}
	/**
	 * 一共多少页
	 *
	 * @param: @return
	 * @return: Integer
	 * @author: 刘天印
	 * @date: 2018年11月28日 下午3:42:33
	 */
	public Integer getTotalPage() {
		//return (allRecords+pageSize-1)/pageSize ;
		return (int)Math.ceil((double)allRecords/pageSize);
	}
	/**
	 * 首页
	 *
	 * @param: @return
	 * @return: Integer
	 * @author: 刘天印
	 * @date: 2018年11月28日 下午3:49:11
	 */
	public Integer getFirst() {
		return 1;
	}
	/**
	 * 上一页
	 *
	 * @param: @return
	 * @return: Integer
	 * @author: 刘天印
	 * @date: 2018年11月28日 下午3:51:46
	 */
	public Integer getPre() {
		if(pageNo == 1) {
			return 1;
		}
		return pageNo -1 ;
	}
	/**
	 * 下一页
	 *
	 * @param: @return
	 * @return: Integer
	 * @author: 刘天印
	 * @date: 2018年11月28日 下午3:51:46
	 */
	public Integer getNext() {
		if(pageNo == getTotalPage()) {
			return getTotalPage();
		}
		return pageNo +1 ;
	}
	/**
	 * 尾页
	 *
	 * @param: @return
	 * @return: Integer
	 * @author: 刘天印
	 * @date: 2018年11月28日 下午3:50:39
	 */
	public Integer getLast() {
		return getTotalPage();
	}
	
	public List<T> getList() {
		return list;
	}
	public void setList(List<T> list) {
		this.list = list;
	}
	public Integer getPageNo() {
		return pageNo;
	}
	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}
	public Integer getPageSize() {
		return pageSize;
	}
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
	public Integer getAllRecords() {
		return allRecords;
	}
	public void setAllRecords(Integer allRecords) {
		this.allRecords = allRecords;
	}
	
	
}
