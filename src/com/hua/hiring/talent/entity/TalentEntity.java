package com.hua.hiring.talent.entity;

public class TalentEntity {
	
	private Integer id ;
	private String code;
	private String password;
	private String name;
	private String phone;
	private String email;
	private Integer age;
	private Integer gender;
	private String hobby;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public Integer getGender() {
		return gender;
	}
	public void setGender(Integer gender) {
		this.gender = gender;
	}
	public String getHobby() {
		return hobby;
	}
	public void setHobby(String hobby) {
		this.hobby = hobby;
	}
	@Override
	public String toString() {
		return "TalentEntity [id=" + id + ", code=" + code + ", password=" + password + ", name=" + name + ", phone="
				+ phone + ", email=" + email + ", age=" + age + ", gender=" + gender + ", hobby=" + hobby + "]";
	}
	
	
	
}
