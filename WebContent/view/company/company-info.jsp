<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>修改企业信息</title>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/static/common/plugin/bootstrap-3.3.5/css/bootstrap.min.css">
	</head>
	<body>
		<h1 align="center">企业信息</h1>
		<hr>
		<form class="form-horizontal" action="<%=request.getContextPath() %>/CompanyServlet?commond=update" method="post" onsubmit="return validate();">
			<div class="form-group">
			    <label for="code" class="col-sm-2 control-label">帐号</label>
			    <div class="col-sm-10">
			    	<input type="text" class="form-control" id="code" name="code" value="${company.code }">
			    	<span id="codeInfo"></span>
			    </div>
		  	</div>
		  	<div class="form-group">
			    <label for="name" class="col-sm-2 control-label">名称</label>
			    <div class="col-sm-10">
			    	<input type="text" class="form-control" id="name" name="name" value="${company.name }">
			    	<span id="nameInfo"></span>
			    </div>
		  	</div>
		  	<div class="form-group">
			    <label for="synopsis" class="col-sm-2 control-label">简介</label>
			    <div class="col-sm-10">
					<textarea class="form-control" rows="5" id="synopsis" name="synopsis">${company.synopsis }</textarea>
			    	<span id="synopsisInfo"></span>
			    </div>
		  	</div>
		  	<div class="form-group">
			    <label for="email" class="col-sm-2 control-label">邮箱</label>
			    <div class="col-sm-10">
			    	<input type="email" class="form-control" id="email" name="email" value="${company.email }">
			    	<span id="emailInfo"></span>
			    </div>
		  	</div>
		  	<div class="form-group">
			    <label for="phone" class="col-sm-2 control-label">联系电话</label>
			    <div class="col-sm-10">
			    	<input type="text" class="form-control" id="phone" name="phone" value="${company.phone }">
			    	<span id="phoneInfo"></span>
			    </div>
		  	</div>
		  	<div class="form-group">
			    <label for="linkman" class="col-sm-2 control-label">联系人</label>
			    <div class="col-sm-10">
			    	<input type="text" class="form-control" id="linkman" name="linkman" value="${company.linkman }">
			    	<span id="linkmanInfo"></span>
			    </div>
		  	</div>
		  	<div class="form-group">
			    <label for="address" class="col-sm-2 control-label">地址</label>
			    <div class="col-sm-10">
			    	<input type="text" class="form-control" id="address" name="address" value="${company.address }">
			    	<span id="addressInfo"></span>
			    </div>
		  	</div>
		  	<div class="form-group">
			    <label for="numbers" class="col-sm-2 control-label">人数</label>
			    <div class="col-sm-10">
			    	<input type="text" class="form-control" id="numbers" name="numbers" value="${company.numbers }">
			    	<span id="numbersInfo"></span>
			    </div>
		  	</div>
		  	<div class="form-group">
			    <label for="slogan" class="col-sm-2 control-label">口号</label>
			    <div class="col-sm-10">
			    	<input type="text" class="form-control" id="slogan" name="slogan" value="${company.slogan }">
			    	<span id="sloganInfo"></span>
			    </div>
		  	</div>
		  	
		  	<div class="form-group">
		  		<div class="col-sm-offset-2 col-sm-10">
		  			<button type="submit" class="btn btn-primary" style="display:block;margin:0 auto">修改</button>
		    	</div>
			</div>
		</form>
		<script src="<%=request.getContextPath() %>/static/common/plugin/jquery.min.js"></script>
		<script src="<%=request.getContextPath() %>/static/common/plugin/bootstrap-3.3.5/js/bootstrap.min.js"></script>
		<script src="<%=request.getContextPath()%>/static/company/js/company-register.js"></script>
	</body>
</html>