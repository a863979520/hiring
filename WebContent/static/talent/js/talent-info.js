/**
 * 年龄回显
 */
for (var i = 10; i <= 80; i++) {
	if (age == i) {
		document.write('<option value="' + i + '" selected="selected">' + i + '</option>');
	} else {
		document.write('<option value="' + i + '">' + i + '</option>');
	}
}

/**
 * 等页面元素加载完成才执行这段JS
 */
window.onload = function() {
	
	/**
	 * 性别回显
	 */
	var genderItems = document.getElementsByName('gender');
	for (var i = 0; i < genderItems.length; i++) {
		if (gender == genderItems[i].value) {
			genderItems[i].checked = true;
			break;
		}
	}
	
	/**
	 * 爱好回显 
	 */
	var hobbies = hobby.split('|');
	var hobbyItems = document.getElementsByName('hobby');
	for (var i = 0; i < hobbies.length; i++) {
		for (var j = 0; j < hobbyItems.length; j++) {
			if (hobbies[i] == hobbyItems[j].value) {
				hobbyItems[j].checked = true;
				break;
			}
		}
	}
}