package com.hua.hiring.company.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hua.hiring.common.util.AppUtil;
import com.hua.hiring.company.dao.PositionDao;
import com.hua.hiring.company.entity.PositionEntity;
import com.hua.hiring.talent.dao.ApplicationDao;
import com.hua.hiring.talent.dao.ResumeDao;
import com.hua.hiring.talent.entity.ApplicationEntity;
import com.hua.hiring.talent.entity.ResumeEntity;

public class PositionServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	private PositionDao positionDao = new PositionDao();
    private ApplicationDao applicationDao = new ApplicationDao();
    private ResumeDao resumeDao = new ResumeDao();

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String commond = request.getParameter("commond");
		if("list".equals(commond)) {
			list(request, response);
		}else if("get".equals(commond)) {
			get(request, response);
		}else if("add".equals(commond)) {
			add(request, response);
		}else if("preupdate".equals(commond)) {
			preupdate(request,response);
		}else if("update".equals(commond)) {
			update(request,response);
		} else if("delete".equals(commond)) {
			delete(request,response);
		}else {
			throw new IllegalStateException("请求非法");
		}
	}
	/**
	 * 列举职位
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	private void list(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<PositionEntity> positionli = positionDao.list();
		request.setAttribute("positionli", positionli);
		request.getRequestDispatcher("view/company/position-list.jsp").forward(request,response);
	}
	/**
	 * 根据id查询到职位的详细信息
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	private void get(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(!AppUtil.islogin(request)) {
			request.setAttribute("sessionClosed", "请先登录或者注册后再进行投递！");
			request.getRequestDispatcher("view/talent/talent-login.jsp").forward(request, response);
		}else {
			
			int id = Integer.parseInt(request.getParameter("id"));
			int talentId = AppUtil.getTalentId(request, response);
			
			PositionEntity position = positionDao.get(id);
			ApplicationEntity application = applicationDao.isApplied(talentId,id);
			List<ResumeEntity> resumeList = resumeDao.list(talentId);
			
			request.setAttribute("position", position);
			request.setAttribute("resumeList", resumeList);
			request.setAttribute("application", application);
			request.getRequestDispatcher("view/company/position-detail.jsp").forward(request,response);
		}
	}
	/**
	 * 职位的新增
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	private void add(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Integer companyId = AppUtil.getCompanyId(request, response);
		String name = request.getParameter("name");
		String duty = request.getParameter("duty");
		String ability = request.getParameter("ability");
		Integer salary1 = Integer.parseInt(request.getParameter("salary1"));
		Integer salary2 = Integer.parseInt(request.getParameter("salary2"));
		//
		PositionEntity position = new PositionEntity();
		position.setCompanyId(companyId);
		position.setName(name);
		position.setDuty(duty);
		position.setAbility(ability);
		position.setSalary1(salary1);
		position.setSalary2(salary2);
		int row = positionDao.add(position);
		// 3
		response.sendRedirect("CompanyServlet?commond=main&row=" + row );
	}
	/**
	 * 职位的修改（先查询到职位的详细信息再修改）
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	private void preupdate(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 1
		int id = Integer.parseInt(request.getParameter("id"));
		// 2
		PositionEntity position = positionDao.preUpdate(id);
		// 3
		if(position == null) {
			request.setAttribute("error", "该职位不存在或用户操作失误~");
			response.sendRedirect("CompanyServlet?commond=main");
		}else {
			request.setAttribute("position", position);
			request.getRequestDispatcher("view/company/position-update.jsp").forward(request, response);
		}
	}
	/**
	 * 职位的修改（执行操作）
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	private void update(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 1
		int id = Integer.parseInt(request.getParameter("id"));
		String name = request.getParameter("name");
		String duty = request.getParameter("duty");
		String ability = request.getParameter("ability");
		int salary1 = Integer.parseInt(request.getParameter("salary1"));
		int salary2 = Integer.parseInt(request.getParameter("salary2"));

		//
		PositionEntity position = new PositionEntity();
		position.setId(id);
		position.setName(name);
		position.setDuty(duty);
		position.setAbility(ability);
		position.setSalary1(salary1);
		position.setSalary2(salary2);
		int row = positionDao.update(position);
		// 3
		response.sendRedirect("CompanyServlet?commond=main&row=" + row );
	}
	/**
	 * 职位的删除
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	private void delete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 1
		int id = Integer.parseInt(request.getParameter("id"));
		// 2
		int row = positionDao.delete(id);
		// 3
		response.sendRedirect("CompanyServlet?commond=main&row=" + row );
		
	}
}
