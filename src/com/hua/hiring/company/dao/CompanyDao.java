package com.hua.hiring.company.dao;

import java.util.ArrayList;
import java.util.List;

import com.hua.hiring.common.util.DBUtil;
import com.hua.hiring.company.entity.CompanyEntity;

public class CompanyDao {
	/**
	 * 公司登录
	 * @param code
	 * @param password
	 * @return
	 */
	public CompanyEntity login(String code, String password) {
		String sql = "SELECT name, id FROM company WHERE code=? AND password=?";
		return DBUtil.getUniqueResult(CompanyEntity.class, sql, code, password);
	}

	/**
	 * 企业的注册
	 *
	 * @param: @param companyEntity
	 * @return: void
	 * @author: 刘天印
	 * @date: 2018年12月1日 上午11:10:52
	 */
	public int regeister(CompanyEntity companyEntity) {
		String sql = "INSERT INTO company VALUE (NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
//		StringBuilder sb = new StringBuilder();
//		sb.append(companyEntity.getCode());
//		sb.append(companyEntity.getPassword());
//		sb.append(companyEntity.getName());
//		sb.append(companyEntity.getSynopsis());
//		sb.append(companyEntity.getEmail());
//		sb.append(companyEntity.getPhone());
//		sb.append(companyEntity.getLinkman());
//		sb.append(companyEntity.getAddress());
//		sb.append(companyEntity.getNumbers());
//		sb.append(companyEntity.getSlogan());
		List<Object> list = new ArrayList<>();
		list.add(companyEntity.getCode());
		list.add(companyEntity.getPassword());
		list.add(companyEntity.getName());
		list.add(companyEntity.getSynopsis());
		list.add(companyEntity.getEmail());
		list.add(companyEntity.getPhone());
		list.add(companyEntity.getLinkman());
		list.add(companyEntity.getAddress());
		list.add(companyEntity.getNumbers());
		list.add(companyEntity.getSlogan());
		
		return DBUtil.executeUpdate(sql, list);
		
	}

	/**
	 * 公司的信息
	 *
	 * @param: @param companyId
	 * @param: @return
	 * @return: CompanyEntity
	 * @author: 刘天印
	 * @date: 2018年12月2日 下午3:48:10
	 */
	public CompanyEntity detail(int companyId) {
		String sql = "SELECT code, password, name, phone, synopsis, email, linkman, address, numbers, slogan FROM company WHERE id =? ";
		return DBUtil.getUniqueResult(CompanyEntity.class, sql, companyId);
	}

	/**
	 * 公司信息的修改
	 *
	 * @param: @param companyEntity
	 * @param: @return
	 * @return: int
	 * @author: 刘天印
	 * @date: 2018年12月2日 下午4:07:35
	 */
	public int update(CompanyEntity company) {
		String sql ="UPDATE company SET code=?, name=?, synopsis=?, email=?, phone=?, linkman=?, address=?, numbers=?, slogan=? WHERE id=?";
		return DBUtil.executeUpdate(sql, company.getCode(),
										company.getName(),
										company.getSynopsis(),
										company.getEmail(),
										company.getPhone(),
										company.getLinkman(),
										company.getAddress(),
										company.getNumbers(),
										company.getSlogan(),
										company.getId()
										);
	}
	/**
	 * 运用AJAX查询了此帐号是否已别占用
	 *
	 * @param: @param code
	 * @param: @return
	 * @return: CompanyEntity
	 * @author: 刘天印
	 * @date: 2018年12月12日 下午6:59:19
	 */
	public CompanyEntity validateCode(String code) {
		String sql = "SELECT code FROM company WHERE code=?";
		return DBUtil.getUniqueResult(CompanyEntity.class, sql, code);
	}
	/**
	 * 修改密码
	* @Description: 该函数的功能描述
	*
	* @param companyId
	* @param passw
	* @return
	* @throws：异常描述
	*
	* @author: 刘天印
	* @date: 2018年12月19日 下午5:07:40 
	*
	 */
	public int changePassword(int companyId, String passw) {
		String sql ="UPDATE company SET password=? WHERE id=?";
		return DBUtil.executeUpdate(sql, passw,companyId);
	}
	
}
