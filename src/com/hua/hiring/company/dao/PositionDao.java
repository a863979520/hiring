package com.hua.hiring.company.dao;

import java.util.ArrayList;
import java.util.List;

import com.hua.hiring.common.util.DBUtil;
import com.hua.hiring.company.entity.PositionEntity;

public class PositionDao {
	/**
	 * 列举职位
	 * @return
	 */
	public List<PositionEntity> list() {
		String sql = "SELECT c.name as 'company$name', c.address as 'company$address', c.slogan as 'company$slogan', p.id, p.name, p.salary1, p.salary2, p.release_time FROM position p, company c WHERE p.company_id = c.id ORDER BY c.id ASC ";
		List<PositionEntity> positionlist = DBUtil.executeQuery(PositionEntity.class, sql);
		return positionlist;
	}
	/**
	 * 职位的新增
	 *
	 * @param: @param positionEntity
	 * @param: @return
	 * @return: int
	 * @author: 刘天印
	 * @date: 2018年11月27日 下午3:23:34
	 */
	public int add(PositionEntity position) {
		String sql = "INSERT INTO position VALUE ( NULL, ?, ?, ?, ?, ?, ?, NOW())";
		List<Object>  parem = new ArrayList<>();
		parem.add(position.getCompanyId());
		parem.add(position.getName());
		parem.add(position.getDuty());
		parem.add(position.getAbility());
		parem.add(position.getSalary1());
		parem.add(position.getSalary2());
		
		int row = DBUtil.executeUpdate(sql,parem);
		return row ;
	}
	/**
	 * 根据id查询到职位的详细信息
	 * @param id
	 * @return
	 */
	public PositionEntity get(int id) {
		String sql ="SELECT c.name as 'company$name', c.email as 'company$email', c.phone as 'company$phone', c.linkman as 'company$linkman', c.address as 'company$address', c.numbers as 'company$numbers', c.slogan as 'company$slogan', p.id, p.name, p.duty, p.ability, p.salary1, p.salary2, p.release_time FROM position p, company c WHERE p.company_id = c.id AND p.id = ?";
		return DBUtil.getUniqueResult(PositionEntity.class, sql, id);
	}
	/**
	 * 根据企业的ID得到它本身的职位信息
	 *
	 * @param: @param companyId
	 * @param: @return
	 * @return: List<PositionEntity>
	 * @author: 刘天印
	 * @date: 2018年11月26日 下午6:45:35
	 */
	public List<PositionEntity> listById(int companyId) {
		String sql = "SELECT id, `name`, salary1, salary2, release_time FROM position WHERE company_id=?";
		return DBUtil.executeQuery(PositionEntity.class, sql, companyId);
	}
	/**
	 * 根据职位的ID删除职位
	 *
	 * @param: @param id
	 * @param: @return
	 * @return: int
	 * @author: 刘天印
	 * @date: 2018年11月26日 下午7:10:55
	 */
	public int delete(int id) {
		String sql = "DELETE FROM position WHERE id=?";
		int row = DBUtil.executeUpdate(sql, id);
		return row;
	}
	/**
	 *  根据企业的ID得到它本身的详细职位信息
	 *
	 * @param: @param id
	 * @param: @return
	 * @return: PositionEntity
	 * @author: 刘天印
	 * @date: 2018年11月26日 下午7:15:07
	 */
	public PositionEntity preUpdate(int id) {
		String sql = "SELECT id, `name`, duty, ability, salary1, salary2, release_time FROM position WHERE id=? ORDER BY id DESC";
		return DBUtil.getUniqueResult(PositionEntity.class, sql, id);
	}
	/**
	 * 简历的修改（执行操作）
	 *
	 * @param: @param resumeEntity
	 * @param: @return
	 * @return: int
	 * @author: 刘天印
	 * @date: 2018年11月20日 下午3:25:08
	 */
	public int update(PositionEntity position) {
		
		String sql ="UPDATE position SET `name`=?, duty=?, ability=?, salary1=?, salary2=? WHERE id=?";
		
		return DBUtil.executeUpdate(sql, position.getName(),position.getDuty(),position.getAbility(),position.getSalary1(),position.getSalary2(), position.getId());
	}
}
