/**
 * 企业注册表单验证
 */
function validate() {
	var flag = true;
	var code = document.getElementById('code').value;
	var password = document.getElementById('password').value;
	var password2 = document.getElementById('password2').value;
	var name = document.getElementById('name').value;
	var synopsis = document.getElementById('synopsis').value;
	var phone = document.getElementById('phone').value;
	var email = document.getElementById('email').value;
	var linkman = document.getElementById('linkman').value;
	var address = document.getElementById('address').value;
	var numbers = document.getElementById('numbers').value;
	var slogan = document.getElementById('slogan').value;
	if (!/^\d{6,12}$/.test(code)) {
		document.getElementById('codeInfo').innerHTML = '帐号必须为6~12位的数字！';
		flag = false;
	} else {
		document.getElementById('codeInfo').innerHTML = '';
	}
	if (password.length < 4 || password.length > 12) {
		document.getElementById('passwordInfo').innerHTML = '密码必须为4~12！';
		flag = false;
	} else {
		document.getElementById('passwordInfo').innerHTML = '';
	}
	if (password != password2) {
		document.getElementById('password2Info').innerHTML = '两次密码输入不一致！';
		flag = false;
	} else {
		document.getElementById('password2Info').innerHTML = '';
	}
	if (name == '') {
		document.getElementById('nameInfo').innerHTML = '用户名不能为空！';
		flag = false;
	} else {
		document.getElementById('nameInfo').innerHTML = '';
	}
	if (synopsis == '') {
		document.getElementById('synopsisInfo').innerHTML = '简介不能为空！';
		flag = false;
	} else {
		document.getElementById('synopsisInfo').innerHTML = '';
	}
	if (phone == '') {
		document.getElementById('phoneInfo').innerHTML = '手机号不能为空！';
		flag = false;
	} else {
		document.getElementById('phoneInfo').innerHTML = '';
	}
	if (email == '') {
		document.getElementById('emailInfo').innerHTML = '邮箱不能为空！';
		flag = false;
	} else {
		document.getElementById('emailInfo').innerHTML = '';
	}
	if (linkman == '') {
		document.getElementById('linkmanInfo').innerHTML = '联系人不能为空！';
		flag = false;
	} else {
		document.getElementById('linkmanInfo').innerHTML = '';
	}
	if (address == '') {
		document.getElementById('addressInfo').innerHTML = '地址不能为空！';
		flag = false;
	} else {
		document.getElementById('addressInfo').innerHTML = '';
	}
	if (numbers == '') {
		document.getElementById('numbersInfo').innerHTML = '人数不能为空！';
		flag = false;
	} else {
		document.getElementById('numbersInfo').innerHTML = '';
	}
	if (slogan == '') {
		document.getElementById('sloganInfo').innerHTML = '口号不能为空！';
		flag = false;
	} else {
		document.getElementById('sloganInfo').innerHTML = '';
	}
	return flag;
}