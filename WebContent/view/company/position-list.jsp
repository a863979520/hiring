<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="icon" href="<%=request.getContextPath() %>/static/common/img/favicon.ico" type="image/x-icon">
		<link rel="stylesheet" href="<%=request.getContextPath() %>/static/common/plugin/bootstrap-3.3.5/css/bootstrap.min.css">
		<title>查询所有的职位</title>
	</head>
	<body>
		<a href="<%=request.getContextPath()%>/view/talent/talent-login.jsp">求职者登录</a> &nbsp;
		<a href="<%=request.getContextPath()%>/view/talent/talent-register.jsp">求职者注册</a> &nbsp;
		<a href="<%=request.getContextPath()%>/view/company/company-login.jsp">企业登录</a> &nbsp;
		<a href="<%=request.getContextPath()%>/view/company/company-register.jsp">企业注册</a> 
		<h2 align="center">最近的职位信息</h2>
		
		<c:forEach items="${positionli }" var="positionli">
			${positionli["company$name"] } &nbsp; "${positionli["company$slogan"] }"  <br>
			<a href="<%=request.getContextPath() %>/PositionServlet?commond=get&id=${positionli.id}">${positionli.name }</a> &nbsp; 
			${positionli.salary1 }--${positionli.salary2 } &nbsp; 
			${positionli["company$address"] } &nbsp; 
			<fmt:formatDate value="${positionli.releaseTime }" pattern="yyyy-MM-dd HH:mm:ss"/><br><br>
		</c:forEach>
		
		<script src="<%=request.getContextPath() %>/static/common/plugin/jquery.min.js"></script>
		<script src="<%=request.getContextPath() %>/static/common/plugin/bootstrap-3.3.5/js/bootstrap.min.js"></script>
	</body>
</html>